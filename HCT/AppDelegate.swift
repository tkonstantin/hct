//
//  AppDelegate.swift
//  HCT
//
//  Created by Константин on 23.10.2017.
//  Copyright © 2017 Константин. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces
import Firebase
import UserNotifications

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        GMSServices.provideAPIKey("AIzaSyDWgj6IQ98HRg5lZSwh0eE8-GqRqwxqs2g")
        GMSPlacesClient.provideAPIKey("AIzaSyDWgj6IQ98HRg5lZSwh0eE8-GqRqwxqs2g")
        FirebaseApp.configure()
        setupRemoteNotifications(for: application)
        Server.makeRequest.getServiceList()
        Server.makeRequest.getFuelList()
        
        if let remoteNotification = launchOptions?[UIApplicationLaunchOptionsKey.remoteNotification] as? [AnyHashable: Any] {
            handlePush(with: remoteNotification)
        }
        
        return true
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        NotificationCenter.default.post(name: becameActiveNotification, object: nil)
    }
    
    var needsOpenChat = false
    
    
    private func setupRemoteNotifications(for application: UIApplication) {
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        
        application.registerForRemoteNotifications()
        updateMessagingToken()
    }
    
    func updateMessagingToken() {
        if let token = Messaging.messaging().fcmToken {
            Server.makeRequest.setPushToken(token: token)
        }
    }

    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any]) {
        handlePush(with: userInfo)
    }

    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        handlePush(with: userInfo)
        completionHandler(.newData)
    }
    
}


extension AppDelegate: MessagingDelegate, UNUserNotificationCenterDelegate {

    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        Server.makeRequest.setPushToken(token: fcmToken)
    }
    
    func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingRemoteMessage) {
        self.handlePush(with: remoteMessage.appData)
    }
    
    
    func handlePush(with userInfo: [AnyHashable: Any]) {
        print("!\n!\n!\nNOTIFICATION: received push with userinfo \(userInfo)\n!\n!\n!")
        
        var isMessage = false
        if let type = userInfo["type"] as? Int, type == 1 {
            isMessage = true
        } else if let type = userInfo["type"] as? String, type == "1" {
            isMessage = true
        }
        
        if isMessage {
            Server.makeRequest.receivedMessagesCount += 1
            NotificationCenter.default.post(name: newMessageNotification, object: nil, userInfo: userInfo)
            if UIApplication.shared.applicationState == .inactive {
                needsOpenChat =  true
            }
        }
    }
  
}
