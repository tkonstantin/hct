//
//  CardNumberFieldInputView.swift
//  HCT
//
//  Created by Константин on 23.10.2017.
//  Copyright © 2017 Константин. All rights reserved.
//

import UIKit

protocol CardNumberInputViewDelegate:class {
    func numericTapped(_ num: Int)
    func deleteTapped()
    func resetPasswordTapped()
}

class CardNumberFieldInputView: UIView {
    
    @IBOutlet weak var resetPasswordLabel: UILabel!
    
    class func createOne(_ delegate: CardNumberInputViewDelegate, canReset: Bool) -> CardNumberFieldInputView {
        let new = UINib(nibName: "CardNumberInputView", bundle: nil).instantiate(withOwner: nil, options: nil).first as! CardNumberFieldInputView
        new.delegate = delegate
        new.resetPasswordLabel.isHidden = !canReset
        return new
    }
    
    
    
    weak var delegate: CardNumberInputViewDelegate?

    @IBAction func symbolTapped(_ sender: UIButton) {
        if sender.tag == -1 {
            delegate?.deleteTapped()
        } else {
            delegate?.numericTapped(sender.tag)
        }
    }
    
    @IBAction func resetPasswordTapped(_ sender: UIButton) {
        delegate?.resetPasswordTapped()
    }
    
}
