//
//  CardNumberField.swift
//  HCT
//
//  Created by Константин on 23.10.2017.
//  Copyright © 2017 Константин. All rights reserved.
//

import UIKit

class CardNumberField: UITextField, CardNumberInputViewDelegate {

    
    var currentNumber: [Int] = []
    var canResetPassword = false
    
    
    override func awakeFromNib() {
        self.inputView = CardNumberFieldInputView.createOne(self, canReset: canResetPassword)
        super.awakeFromNib()
    }
    
    func numericTapped(_ num: Int) {
        let range = NSRange.init(location: self.text!.count, length: 0)
        if self.delegate?.textField!(self, shouldChangeCharactersIn: range, replacementString: String(num)) ?? false {
            self.text! += String(num)
            currentNumber.append(num)
        }
    }
    
    func deleteTapped() {
        guard self.text != nil && self.text!.count != 0 else { return }
        let range = NSRange.init(location: self.text!.count - 1, length: 1)
        if self.delegate?.textField!(self, shouldChangeCharactersIn: range, replacementString: "") ?? false {
            self.text = String(self.text!.dropLast())
            if currentNumber.count != 0 {
                _ = currentNumber.removeLast()
            }
        }
    }
    
    private var resetPasswordBlock: (()->Void)?=nil
    
    func onResetPasswordTapped(block: (()->Void)?) {
        canResetPassword = true
        (self.inputView as? CardNumberFieldInputView)?.resetPasswordLabel?.isHidden = false
        resetPasswordBlock = block
    }
    
    func resetPasswordTapped() {
        resetPasswordBlock?()
    }

}
