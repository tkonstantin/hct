//
//  NavTitleView.swift
//  HCT
//
//  Created by Константин on 03.11.2017.
//  Copyright © 2017 Константин. All rights reserved.
//

import UIKit

class NavTitleView: UIView {
    override var intrinsicContentSize: CGSize {
        get {
            self.clipsToBounds = true
            (self.subviews.first as? UIImageView)?.clipsToBounds = true
            (self.subviews.first as? UIImageView)?.contentMode = .scaleAspectFit
            (self.subviews.first as? UIImageView)?.frame = CGRect(origin: .zero, size: CGSize(width: 88, height: 28))
            return CGSize(width: 88, height: 28)
        }
    }
    
    override func didMoveToSuperview() {
        self.clipsToBounds = true
        (self.subviews.first as? UIImageView)?.clipsToBounds = true
        (self.subviews.first as? UIImageView)?.contentMode = .scaleAspectFit
        (self.subviews.first as? UIImageView)?.frame = CGRect(origin: .zero, size: CGSize(width: 88, height: 28))
        self.frame.size =  CGSize(width: 88, height: 28)
    }
    
    
}
