//
//  AutocompletableTextField.swift
//  HCT
//
//  Created by Константин on 31.10.2017.
//  Copyright © 2017 Константин. All rights reserved.
//

import UIKit
import GooglePlaces

protocol AutocompletableTextFieldDelegate: class {
    func autocomplete_didSelect(coordinate: CLLocationCoordinate2D?)
}

class AutocompletableTextField: ShadowedView {
    
    weak var delegate: AutocompletableTextFieldDelegate?
    
    private var autocompleteFetcher: GMSAutocompleteFetcher!
    private var predictions: [GMSAutocompletePrediction] = []
    private var stationInfo: MapStationInfo?
    private var autocompleteBounds: GMSCoordinateBounds!
    
    func configure(delegate d: AutocompletableTextFieldDelegate, autocompleteBounds bounds: GMSCoordinateBounds, stationInfo info: MapStationInfo?=nil) {
        delegate = d
        stationInfo = info
        autocompleteBounds = bounds
    }
    
     func updatePredictions(_ new: [GMSAutocompletePrediction]) {
        tableHeight.constant = CGFloat(new.count)*44
        self.setNeedsLayout()
        self.layoutIfNeeded()
        predictions = new
        self.tableView.reloadSections([0], with: .automatic)
    }
    
    @IBOutlet weak var tableHeight: NSLayoutConstraint!
    @IBOutlet weak var tableView: UITableView! {
        didSet {
            tableView.delegate = self
            tableView.dataSource = self
        }
    }
    @IBOutlet weak var textField: UITextField! {
        didSet {
            textField.delegate = self
            textField.autocorrectionType = .no
        }
    }
    
    
    override func awakeFromNib() {
        autocompleteFetcher = GMSAutocompleteFetcher(bounds: nil, filter: nil)
        autocompleteFetcher.delegate = self
    }
    
    var shouldSendRequests = true
    
    
    
}

extension AutocompletableTextField: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return predictions.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let currentPrediction = predictions[indexPath.row]
        let cell = UITableViewCell()
        cell.textLabel?.attributedText = currentPrediction.attributedFullText
        cell.textLabel?.textAlignment = .center
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let currentPrediction = predictions[indexPath.row]
        
        shouldSendRequests = false
        textField.attributedText = currentPrediction.attributedFullText
        asyncAfter {
            
            self.shouldSendRequests = true
            
            self.updatePredictions([])
            guard let pid = currentPrediction.placeID else { return }
            GMSPlacesClient().lookUpPlaceID(pid) { (place, error) in
                if error != nil {
                    print("GMSPlacesClient lookUpPlaceID for placeID \(pid) error = \(error)")
                }
                self.endEditing(true)
                self.delegate?.autocomplete_didSelect(coordinate: place?.coordinate)
            }
        }
        
    }
}

extension AutocompletableTextField: GMSAutocompleteFetcherDelegate {
    
    func presentAutocomplete() {
        autocompleteFetcher = GMSAutocompleteFetcher.init(bounds: autocompleteBounds, filter: nil)
        autocompleteFetcher.delegate = self
    }
    
    func didAutocomplete(with predictions: [GMSAutocompletePrediction]) {
        self.updatePredictions(predictions)
    }
    
    func didFailAutocompleteWithError(_ error: Error) {
        print("didFailAutocompleteWithError = \(error)")
    }
}


extension AutocompletableTextField: UITextFieldDelegate {
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        presentAutocomplete()
        return true
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        self.updatePredictions([])
        self.stationInfo?.hide{}
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard shouldSendRequests else { return false }
        let newtext = ((textField.text ?? "") as NSString).replacingCharacters(in: range, with: string)
        autocompleteFetcher?.sourceTextHasChanged(newtext)
        return true
    }
}
