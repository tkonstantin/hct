//
//  ShadowedView.swift
//  Heloo2017
//
//  Created by Константин on 24.09.17.
//  Copyright © 2017 Константин. All rights reserved.
//

import UIKit

class ShadowedView: UIView {
    
    override func awakeFromNib() {

        self.layer.shadowOffset = CGSize(width: 1, height: 1)
        self.layer.shadowRadius = 6
        self.layer.shadowOpacity = 1
        self.layer.shadowColor = UIColor(hex: "36393C").withAlphaComponent(0.13).cgColor
        self.layer.masksToBounds = false
        

    }
    
}
