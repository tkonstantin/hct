//
//  Presenter.swift
//  Smolnyi
//
//  Created by Константин Черкашин on 30.08.16.
//  Copyright © 2016 Константин Черкашин. All rights reserved.
//

import Foundation
import UIKit

let redColor = UIColor(hex: "FF5722")
let darkGrayColor = UIColor(hex: "525A62")
let transactionGreenColor = UIColor(hex: "58A800")
let transactionRedColor = UIColor(hex: "8A2C0E")


class Presenter {
    
    static let instance = Presenter()
    
    
    let mainDarkGrayColor = UIColor(red: 152/255, green: 152/255, blue: 152/255, alpha: 1.0)
    
    
    
    
    //MARK: - Simple alert
    func showAlert(title: String, message: String, completionHandler: @escaping ()->()) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let ok = UIAlertAction(title: "OK", style: .default, handler: {_ in
            completionHandler()
        })
        alert.addAction(ok)
        UIApplication.shared.keyWindow?.rootViewController?.present(alert, animated: true, completion: nil)
        
    }
    
    func showErrorAlert(_ title: String?, message: String, _completion: @escaping (()->())={}) {
        let alert = UIAlertController(title: title == nil ? "Ошибка" : title!, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: {_ in
            _completion()
        }))
        UIApplication.shared.keyWindow?.rootViewController?.present(alert, animated: true, completion: nil)
    }
    
    func setClearColorAppearance() {
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedStringKey.foregroundColor : UIColor(red: 1, green: 1, blue: 1, alpha: 1), NSAttributedStringKey.font : UIFont.systemFont(ofSize: 18, weight: UIFont.Weight.medium)];
        UINavigationBar.appearance().tintColor = UIColor(red: 1, green: 1, blue: 1, alpha: 1);
        UINavigationBar.appearance().setBackgroundImage(UIImage(), for: UIBarMetrics.default);
        UINavigationBar.appearance().shadowImage = UIImage();
        UINavigationBar.appearance().barTintColor = UIColor.clear
        UINavigationBar.appearance().backgroundColor = UIColor.clear
        
        
        if let statWindow = UIApplication.shared.value(forKey:"statusBarWindow") as? UIView {
            let statusBar = statWindow.subviews[0]
            statusBar.backgroundColor = .clear
        }
        
        UINavigationBar.appearance().backgroundColor = UIColor.clear
        UINavigationBar.appearance().isTranslucent = true
        UINavigationBar.appearance().isOpaque = false
        UIApplication.shared.statusBarStyle = .lightContent
    }
    
    
    
}

// MARK: - Custom storyboard properties
@IBDesignable extension UIView {
    @IBInspectable var borderColor:UIColor? {
        set {
            layer.borderColor = newValue!.cgColor
        }
        get {
            if let color = layer.borderColor {
                return UIColor(cgColor:color)
            }
            else {
                return nil
            }
        }
    }
    @IBInspectable var borderWidth:CGFloat {
        set {
            layer.borderWidth = newValue
        }
        get {
            return layer.borderWidth
        }
    }
    @IBInspectable var cornerRadius:CGFloat {
        set {
            layer.cornerRadius = newValue
//            clipsToBounds = newValue > 0
        }
        get {
            return layer.cornerRadius
        }
    }
//
//    @IBInspectable var maskingTop: Bool {
//        set {
//            self.layer.maskedCorners = (maskingTop ? [.layerMinXMinYCorner, .layerMaxXMinYCorner ] : [])  + (maskingBottom ? [.layerMaxXMinYCorner, .layerMaxXMaxYCorner] : [])
//        } get {
//            return self.layer.maskedCorners.contains(.layerMinXMinYCorner) || self.layer.maskedCorners.contains(.layerMaxXMinYCorner)
//        }
//    }
//
//    @IBInspectable var maskingBottom: Bool {
//        set {
//            self.layer.maskedCorners = (maskingTop ? [.layerMinXMinYCorner, .layerMaxXMinYCorner ] : [])  + (maskingBottom ? [.layerMaxXMinYCorner, .layerMaxXMaxYCorner] : [])
//        } get {
//            return self.layer.maskedCorners.contains(.layerMaxXMinYCorner) || self.layer.maskedCorners.contains(.layerMaxXMaxYCorner)
//        }
//    }

}


@IBDesignable class ShadowedButton: UIButton {
    override func awakeFromNib() {
        self.layer.shadowColor = UIColor(hex: "1F77BD").cgColor
        self.layer.shadowOffset = CGSize(width: -5, height: 5)
        self.layer.shadowRadius = 14 //blur
        self.layer.masksToBounds = false
        self.layer.shadowPath = UIBezierPath(rect: CGRect(x: 0, y: 0, width: self.bounds.width, height: self.bounds.height)).cgPath
        self.layer.shadowOpacity = 0.56
    }
}

extension UIView {
    func firstResponderView() -> UIView? {
        if self.isFirstResponder {
            return self
        }
        for subview in self.subviews {
            if let responder = subview.firstResponderView() {
                return responder
            }
        }
        return nil
    }
    
}


//MARK: - Keyboard appearance's logic
extension UIViewController {
    
    
    func addTapOutsideGestureRecognizer() {
        let tapOutSide = UITapGestureRecognizer(target: self, action: #selector(self.hideKeyboard))
        tapOutSide.cancelsTouchesInView = false
        self.view.addGestureRecognizer(tapOutSide)
    }
    
    func subscribeToKeyboard() {
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillBeHidden), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillBeShown(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillBeShown(_:)), name: NSNotification.Name.UIKeyboardWillChangeFrame, object: nil)
    }
    
    @objc func hideKeyboard() {
        self.view.endEditing(true)
    }

    
    @objc func keyboardWillBeShown (_ notification: Notification) {
        let info = (notification as NSNotification).userInfo
        if let keyboardSize = (info?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            UIView.animate(withDuration: 0.3, animations: {
                self.view.frame.origin.y = -keyboardSize.height
            })
        }
    }
    
    @objc func keyboardWillBeHidden() {
        UIView.animate(withDuration: 0.3, animations: {
            self.view.frame.origin.y = 0
        })
    }
    
    
    
    
}

extension UIImage {
    func requiredStatusBarStyle() -> UIStatusBarStyle {
        var bitmap = [UInt8](repeating: 0, count: 4)
        
            // Get average color.
            let context = CIContext()
            let inputImage: CIImage = ciImage ?? CoreImage.CIImage(cgImage: cgImage!)
            let extent = inputImage.extent
            let inputExtent = CIVector(x: extent.origin.x, y: extent.origin.y, z: extent.size.width, w: extent.size.height)
            let filter = CIFilter(name: "CIAreaAverage", withInputParameters: [kCIInputImageKey: inputImage, kCIInputExtentKey: inputExtent])!
            let outputImage = filter.outputImage!
            let outputExtent = outputImage.extent
            assert(outputExtent.size.width == 1 && outputExtent.size.height == 1)
            
            context.render(outputImage, toBitmap: &bitmap, rowBytes: 4, bounds: CGRect(x: 0, y: 0, width: 1, height: 1), format: kCIFormatRGBA8, colorSpace: CGColorSpaceCreateDeviceRGB())
        
        let brightness = ((CGFloat(bitmap[0])/255.0)*299 + (CGFloat(bitmap[1])/255.0)*587 + (CGFloat(bitmap[2])/255.0)*114)/1000
    
        return brightness < 0.5 ? .lightContent : .default
    }
}


@IBDesignable class TailIndentLabel: UILabel {
    
    @IBInspectable var isRightInset: Bool = true {
        didSet {
            update()
        }
    }
    @IBInspectable var inset: CGFloat = 0 {
        didSet {
            update()
        }
    }
    
    
    func update() {
        let paragraph = NSMutableParagraphStyle()
        if isRightInset {
            paragraph.baseWritingDirection = .rightToLeft
        }
        paragraph.firstLineHeadIndent = inset
        paragraph.alignment = .left
        self.attributedText = NSAttributedString(string: self.text!, attributes: [NSAttributedStringKey.paragraphStyle : paragraph])
    }
    
}

extension UIColor {
    convenience init(hex: String) {
        let color: UIColor = {
            var cString:String = hex.trimmingCharacters(in: (NSCharacterSet.whitespacesAndNewlines as NSCharacterSet) as CharacterSet).uppercased()
            
            if (cString.hasPrefix("#")) {
                cString = cString.substring(from: cString.characters.index(cString.startIndex, offsetBy: 1))
            }
            
            if ((cString.characters.count) != 6) {
                return UIColor.gray
            }
            
            var rgbValue:UInt32 = 0
            Scanner(string: cString).scanHexInt32(&rgbValue)
            
            return UIColor(
                red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
                green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
                blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
                alpha: CGFloat(1.0)
            )
        }()
        
        self.init(cgColor: color.cgColor)
    }
}

class RefreshableTable: UITableView {
    
    private var tableViewRefreshControlCompletionBlock: ((UIRefreshControl?)->Void)?
    private var currentRefreshControl: UIRefreshControl?
    
    @objc private func callingRefresh() {
        tableViewRefreshControlCompletionBlock?(currentRefreshControl)
    }
    
    func addPullToRefresh(color: UIColor = .white, _ completion: @escaping (UIRefreshControl?)->Void) {
        let control = UIRefreshControl()
        control.tintColor = color
        control.attributedTitle = NSAttributedString(string: "Потяни, чтобы обновить", attributes: [NSAttributedStringKey.foregroundColor: color])
        control.addTarget(self, action: #selector(self.callingRefresh), for: .valueChanged)
        tableViewRefreshControlCompletionBlock = completion
        currentRefreshControl = control
        self.addSubview(control)
    }
}
