//
//  AppendedCardViewController.swift
//  HCT
//
//  Created by Константин on 24.10.2017.
//  Copyright © 2017 Константин. All rights reserved.
//

import UIKit

let newMessageNotification: Notification.Name = Notification.Name.init("newMessageNotification")
let becameActiveNotification: Notification.Name = Notification.Name.init("becameActiveNotification")


class AppendedCardViewController: UIViewController {
    
    var needsOpenChat = false
    
    @IBOutlet weak var break_blockedView: BlockedView!
    @IBOutlet weak var user_blockedView: BlockedView!
    @IBOutlet weak var blockedView: BlockedView!
    
    @IBOutlet weak var cardBalance: UILabel!
    @IBOutlet weak var cardNumber: UILabel!
    @IBOutlet weak var cardRequestButton: UIButton!
    @IBOutlet weak var cardmenuButton: UIButton!
    @IBOutlet weak var blockedCardmenuButton: UIButton!
    @IBOutlet weak var carLogo: UIImageView!
    @IBOutlet weak var cardView: UIView!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var tableItems: UITableView!
    @IBOutlet weak var messagesView: UIView!
    @IBOutlet weak var messagesLabel: UILabel!
    @IBOutlet weak var scrollToTopView: UIView!
    
    class func createOne() -> (UINavigationController, AppendedCardViewController?) {
        let new = UIStoryboard(name: "AppendedCard", bundle: nil).instantiateInitialViewController() as! UINavigationController
        return (new, new.viewControllers.first as? AppendedCardViewController)
    }
    
    var card: Card? {
        get {
            return UserDataManager.current.card
        }
    }
    var transactions: [Date: [Transaction]] = [:]
    var refresher: UIRefreshControl!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        subscribeForNewMessages()
        subscribeForBecameActive()
        tableItems.delegate = self
        tableItems.dataSource = self
        tableItems.tableFooterView = UIView()
        asyncAfter {
            self.tableItems.tableHeaderView?.frame.size.height = 51 + self.cardView.bounds.size.height
        }
        
        refresher = UIRefreshControl(frame: CGRect(origin: .zero, size: CGSize(width: self.view.bounds.width, height: 44)))
        refresher.addTarget(self, action: #selector(self.reload), for: .valueChanged)
        tableItems.addSubview(refresher)
        
        self.updateViews()
        self.reload()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.updateViews()

        (UIApplication.shared.delegate as? AppDelegate)?.updateMessagingToken()
        
        if needsOpenChat {
            needsOpenChat = false
            self.feedbackMessage()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.updateViews()

    }
    
    private func subscribeForNewMessages() {
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateWithMessage), name: newMessageNotification, object: nil)
    }
    
    private func subscribeForBecameActive() {
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateOnBecameActive), name: becameActiveNotification, object: nil)
    }
    
    @objc private func updateWithMessage() {
        asyncAfter {
            self.updateViews()
            
            if ((UIApplication.shared.delegate as? AppDelegate)?.needsOpenChat ?? false) {
                (UIApplication.shared.delegate as? AppDelegate)?.needsOpenChat = false
                self.feedbackMessage()
            }
        }
    }
    
    @objc private func updateOnBecameActive() {
        asyncAfter {
            self.updateViews()
        }
    }
    
    
    @objc func reload() {
        Server.makeRequest.getUser(user_id: UserDataManager.current.user!.id) {
            self.updateViews()
            self.transactions = [:]
            self.isEOF = false
            self.tableItems.reloadData()
            self.currentPage = -1
            self.isRequestingNextPage = false
            self.loadTransactions()
        }
    }
    
    private func updateViews() {
        
        self.blockedView.isHidden = !(self.card?.isBlockedBySystem ?? false)
        self.user_blockedView.isHidden = !(self.card?.isBlockedByUser ?? false)
        self.break_blockedView.isHidden = !(self.card?.isServiceBreaked ?? false)
        
        self.user_blockedView.cardNumberLabel?.text = String(rawNumber: self.card?.number ?? "")
        self.break_blockedView.cardNumberLabel?.text = String(rawNumber: self.card?.number ?? "")
        
        self.cardNumber.text = String(rawNumber: self.card?.number ?? "")
        self.cardBalance.text = String(self.card?.balance ?? 0) + " " + (card?.currency ?? "")
        self.userName.text = self.card?.name
        
        let receivedMessagesCount = Server.makeRequest.receivedMessagesCount
        self.messagesView.isHidden = receivedMessagesCount == 0
        self.messagesLabel.text = String(receivedMessagesCount)
    }
    
    var currentPage = -1
    var isRequestingNextPage = false
    
    var footerView: UIView {
        get {
            let footer = UIView(frame: CGRect(origin: .zero, size: CGSize(width: self.view.bounds.width, height: 64)))
            let label = UILabel(frame: CGRect(x: 0, y: 32, width: self.view.bounds.width, height: 32))
            label.text = "Загрузка..."
            label.textAlignment = .center
            footer.addSubview(label)
            let activity = UIActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: self.view.bounds.width, height: 44))
            activity.activityIndicatorViewStyle = .gray
            activity.startAnimating()
            footer.addSubview(activity)
            return footer
        }
    }
    
    
    var isEOF = false
    
    func loadTransactions() {
        guard !isRequestingNextPage, !isEOF else { return }
        asyncAfter {
            self.tableItems.tableFooterView = self.footerView
        }
        isRequestingNextPage = true
        currentPage += 1
        
        Server.makeRequest.getTransactions(page: currentPage) { (result) in
            self.isEOF = result.count < 10
            for trx in result {
                if let key = self.transactions.keys.first(where: {$0 == trx.date.dateToZero}) {
                    self.transactions[key]?.append(trx)
                } else {
                    self.transactions[trx.date.dateToZero] = [trx]
                }
            }
            self.needsToUpdateTableView = true
            self.finishUILoading()
            
            asyncAfter {
                self.refresher.endRefreshing()
            }
        }
    }
    
    func finishUILoading() {
        guard needsToUpdateTableView && self.tableItems.tableFooterView != nil && !self.tableItems.isDragging && !self.tableItems.isDecelerating else {
            asyncAfter(milliseconds: 300, { self.finishUILoading() })
            return }
        self.tableItems.contentOffset.y -= self.tableItems.tableFooterView?.frame.size.height ?? 0
        self.tableItems.tableFooterView = nil
        self.tableItems.reloadData()
        self.needsToUpdateTableView = false
        self.isRequestingNextPage = false
    }
    
    var needsToUpdateTableView = true
    
    
    @IBAction func showCardMenu(_ sender: UIButton) {
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        alert.addAction(UIAlertAction(title: "Выйти", style: .destructive, handler: { (_) in
            UserDataManager.current.reset()
            self.tabBarController?.viewControllers?[0] = CardViewController.createOne()
        }))
        
        
        if !((card?.isBlockedByUser ?? false) || (card?.isBlockedBySystem ?? false)) {
            
            alert.addAction(UIAlertAction(title: "Изменить пароль", style: .default, handler: { (_) in
                self.changePassword()
            }))
            
            alert.addAction(UIAlertAction(title: "Заблокировать карту", style: .destructive, handler: { (_) in
                Server.makeRequest.blockCurrentCard(completion: { (success) in
                    self.reload()
                })
            }))
        }
        
        
        
        
        
        alert.addAction(UIAlertAction(title: "Отмена", style: .cancel, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
        
    }
    
    @IBAction func chatTapped(_ sender: UIButton) {
        self.feedbackMessage()
        
    }
    
    @IBAction func requestRecharge(_ sender: UIButton) {
        let alert = UIAlertController(title: "Запрос пополнения", message: "Введите сумму запроса", preferredStyle: .alert)
        alert.view.tintColor = redColor
        alert.addTextField { (field) in
            field.textAlignment = .center
            field.placeholder = "Введите сумму..."
            field.keyboardType = .numberPad
            field.delegate = self
        }
        
        currentFieldMode = .recharge
        sendAction = UIAlertAction(title: "Запросить пополнение", style: .default, handler: { (_) in
            guard let txt = alert.textFields?.first?.text, !txt.isEmpty else { return }
            Server.makeRequest.refill(summ: txt, completion: { (success, errorMessage) in
                if !success {
                    Presenter.instance.showErrorAlert(nil, message: errorMessage!)
                }
            })
            
        })
        sendAction.isEnabled = false
        alert.addAction(sendAction)
        alert.addAction(UIAlertAction(title: "Отмена", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        self.scrollToTopView.isHidden = scrollView.contentOffset.y <= -64
    }
    
    @IBAction func scrollToTopTapped(_ sender: UIButton) {
        self.tableItems.scrollRectToVisible(CGRect(origin: .zero, size: CGSize(width: self.view.bounds.width, height: 20)), animated: true)
    }
    
    
    private enum fieldModes {
        case feedback
        case recharge
        case password
    }
    
    private var currentFieldMode: fieldModes!
    private var sendAction: UIAlertAction!
    
    private func feedbackMessage() {
        self.view.endEditing(true)
        let chat = ChatViewController.makeOne()
        self.navigationController?.pushViewController(chat, animated: true)
    }
    
    private func changePassword() {
        self.tabBarController?.viewControllers?[0] = PasswordViewController.makeOne(in: .change).0
    }
}

extension AppendedCardViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return transactions.keys.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return transactions[Array(transactions.keys.sorted().reversed())[section]]!.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let keys = Array(transactions.keys)
        guard keys.count > indexPath.section, let sectionTransactions = (transactions[keys.sorted().reversed()[indexPath.section]]) else { return UITableViewCell() }
        guard sectionTransactions.count > indexPath.row else { return UITableViewCell() }
        
        let currentTransaction = sectionTransactions[indexPath.row]
        
        let cell = tableItems.dequeueReusableCell(withIdentifier: "cell") as! TransactionTableViewCell
        cell.update(with: currentTransaction)
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.section == tableView.numberOfSections - 1 && indexPath.row == transactions[Array(transactions.keys.sorted().reversed())[indexPath.section]]!.count - 1 {
            self.loadTransactions()
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = UIView(frame: CGRect.init(origin: .zero, size: CGSize(width: tableView.bounds.width, height: 34)))
        
        let leftLabel = UILabel(frame: CGRect(x: 16, y: 8, width: tableView.bounds.width-32, height: 26))
        leftLabel.font = UIFont.systemFont(ofSize: 13)
        leftLabel.textAlignment = .left
        leftLabel.textColor = .black
        leftLabel.text = section == 0 ? "Транзакции" : ""
        header.addSubview(leftLabel)
        
        let rightLabel = UILabel(frame: CGRect(x: 16, y: 8, width: tableView.bounds.width-32, height: 26))
        rightLabel.font = UIFont.systemFont(ofSize: 12)
        rightLabel.textAlignment = .right
        rightLabel.textColor = darkGrayColor
        rightLabel.text = Array(transactions.keys.sorted().reversed())[section].parseDateShort(dots: true)
        header.addSubview(rightLabel)
        
        return header
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 34
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return .leastNonzeroMagnitude
    }
}

extension AppendedCardViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if currentFieldMode != .password {
            let newText = ((textField.text ?? "") as NSString).replacingCharacters(in: range, with: string)
            sendAction?.isEnabled = !newText.isEmpty
        }
        return true
    }
}
