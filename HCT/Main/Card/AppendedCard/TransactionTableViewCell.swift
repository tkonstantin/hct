//
//  TransactionTableViewCell.swift
//  HCT
//
//  Created by Константин on 24.10.2017.
//  Copyright © 2017 Константин. All rights reserved.
//

import UIKit

class TransactionTableViewCell: UITableViewCell {

    @IBOutlet weak var addText: UILabel!
    @IBOutlet weak var sumLabel: UILabel!
    @IBOutlet weak var commentLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!

    func update(with t: Transaction) {
        sumLabel.textColor = t.rightSideColor
        
        let summPreffix: String = {
            if t.isPositive {
                return  "+ "
            } else if t.isNegative {
                 return "- "
            } else {
                return ""
            }
        }()
        
        sumLabel.text = summPreffix + String(t.sum) + " " + t.currency
        
        commentLabel.text = t.comment
        commentLabel.font = UIFont.systemFont(ofSize: 14, weight: t.type == .bonused ? UIFont.Weight.medium : UIFont.Weight.regular)

        timeLabel.text = t.date.parseTime()
        addText.text = t.addText
    }
}



