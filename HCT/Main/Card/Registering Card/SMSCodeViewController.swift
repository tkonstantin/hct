//
//  SMSCodeViewController.swift
//  HCT
//
//  Created by Константин on 24.10.2017.
//  Copyright © 2017 Константин. All rights reserved.
//

import UIKit

class SMSCodeViewController: UIViewController {

    @IBOutlet weak var codeField: CardNumberField!
    @IBOutlet weak var resendButton: UIButton!
    @IBOutlet weak var numberLabel: UILabel! {
        didSet {
            numberLabel.text = "Введите код подтверждения отправленный на номер " + "+7981******99"
        }
    }
    
    var needsOpenChat = false
    var cap: String!
    var pan: String!
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.perform(#selector(self.activateButton), with: nil, afterDelay: 15)
        self.codeField.delegate = self
        self.codeField.becomeFirstResponder()
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addTapOutsideGestureRecognizer()
    }
    
    @IBAction func resendCode(_ sender: UIButton) {
        
    }
    
    @objc func activateButton() {
        resendButton.isEnabled = true
    }
    

    func updateText() {
        codeField.text = String(numbers: codeField.currentNumber)
        if codeField.currentNumber.count == 4 {
            self.view.endEditing(true)
            Server.makeRequest.regStepTwo(cap: cap, code: codeField.currentNumber.map({String($0)}).joined(separator: ""), completion: { (success, message, user_id) in
                if !success {
                    Presenter.init().showErrorAlert(nil, message: message ?? "Неизвестная ошибка")
                    self.codeField.currentNumber = []
                    self.codeField.text = String(numbers: [])
                } else {
                    UserDataManager.current.pan = self.pan

                    Server.makeRequest.getUser(user_id: user_id!, completion: {
                        let new = PasswordViewController.makeOne()
                        new.1?.needsOpenChat = self.needsOpenChat
                        self.needsOpenChat = false
                        self.tabBarController?.viewControllers?[0] = new.0
                    })
                }

            })
        }
    }
    
}

extension SMSCodeViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if  string.count == 1 {
            codeField.currentNumber.append(Int(string)!)
            updateText()
        } else if string.count == 0 && codeField.currentNumber.count > 0 {
            _ = codeField.currentNumber.removeLast()
            updateText()
        }
        
        return false
    }
}


extension String {
    fileprivate init(numbers: [Int]) {
        var first: Int?
        var second: Int?
        var third: Int?
        var fourth: Int?
        
        if numbers.count > 0 {
            first = numbers[0]
            if numbers.count > 1 {
                second = numbers[1]
                if numbers.count > 2 {
                    third = numbers[2]
                    if numbers.count > 3 {
                        fourth = numbers[3]
                    }
                }
            }
        }
        
        var stringArr: [String] = []
        stringArr.append(first == nil ? "-" : String(first!))
        stringArr.append(second == nil ? "-" : String(second!))
        stringArr.append(third == nil ? "-" : String(third!))
        stringArr.append(fourth == nil ? "-" : String(fourth!))
        self = stringArr.joined(separator: " ")

        
    }
    
     init(rawNumber: String) {
        var numbers = rawNumber.map({String($0)})
        
        var result: String = ""
        for index in 0..<numbers.count {
            result.append(numbers[index])
            if index != 0 && (index + 1) % 3 == 0 {
                result.append(" ")
            }
        }
        self =  result

    }
    
    
    
}
