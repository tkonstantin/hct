//
//  CardViewController.swift
//  HCT
//
//  Created by Константин on 23.10.2017.
//  Copyright © 2017 Константин. All rights reserved.
//

import UIKit

class CardViewController: UIViewController {
    
    class func createOne() -> UINavigationController {
        return UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CardNavigation") as! UINavigationController
    }


    @IBOutlet weak var numberField: CardNumberField!
    @IBOutlet weak var regButton: UIButton!
    @IBOutlet weak var cardView: ShadowedView!
    @IBOutlet weak var cardioButton: UIButton!
    
    var needsOpenChat = false
    
    var isValid = false {
        didSet {
            regButton.isEnabled = isValid
            cardView.borderWidth = isValid ? 2 : 0
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        numberField.delegate = self
        self.addTapOutsideGestureRecognizer()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        numberField.becomeFirstResponder()
    }
    
    var inspacingPositions: [Int] = [3, 7, 11]
    var outspacingPositions: [Int] = [5, 9, 13]
    
    
    @IBAction func showCardIo(_ sender: UIButton) {
//        let cardIOVC = CardIOPaymentViewController(paymentDelegate: self)!
//        cardIOVC.modalPresentationStyle = .formSheet
//        cardIOVC.disableManualEntryButtons = true
//        cardIOVC.collectCVV = false
//        cardIOVC.collectExpiry = false
//        self.present(cardIOVC, animated: true, completion: nil)
    }
    
    var currentCapCode: String?
    var currentCardNumber: String?
    
    @IBAction func sendRequest(_ sender: UIButton) {
        var cardNumber = ""
        self.numberField.text!.components(separatedBy: " ").forEach({cardNumber += $0})
        
        Server.makeRequest.regStepOne(card: cardNumber) { (success, errorMessage, capCode) in
            self.currentCardNumber = cardNumber
            self.currentCapCode = capCode

            if success {
                self.performSegue(withIdentifier: "sms", sender: nil)
            } else {
                let alert = UIAlertController(title: "Ошибка", message: errorMessage ?? "Попробуйте позже", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let dest = segue.destination as? SMSCodeViewController {
            dest.cap = currentCapCode!
            dest.pan = currentCardNumber!
            dest.needsOpenChat = self.needsOpenChat
            self.needsOpenChat = false
        }
    }
    
    
}

extension CardViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if inspacingPositions.contains(textField.text!.count) && string.count == 1 {
            textField.text! += " " + string
            return false
        } else if outspacingPositions.contains(textField.text!.count) && string.count == 0 {
            textField.text! = String(textField.text!.dropLast().dropLast())
            return false
        } else if textField.text!.count == 1 && string.count == 0 {
            textField.endEditing(true)
            return true
        }
        
        let newText = (textField.text! as NSString).replacingCharacters(in: range, with: string)
        self.isValid = newText.count >= 11
        return newText.count < 20
    }

}
