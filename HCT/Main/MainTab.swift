//
//  MainTab.swift
//  HCT
//
//  Created by Константин on 23.10.2017.
//  Copyright © 2017 Константин. All rights reserved.
//

import UIKit

class MainTab: UITabBarController {
    
    class func createOne() -> MainTab {
        let new = UIStoryboard(name: "Main", bundle: nil).instantiateInitialViewController() as! MainTab
        return new
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.selectedIndex = 1
        
        if UserDataManager.current.card != nil {
            self.viewControllers?[0] = PasswordViewController.makeOne(in: .enter).0
        }
        if (UIApplication.shared.delegate as? AppDelegate)?.needsOpenChat ?? false {
            openChat(true)
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.openChat), name: newMessageNotification, object: nil)
        
    }
    
    
    @objc private func openChat(_ forced: Bool = false) {
        if ((UIApplication.shared.delegate as? AppDelegate)?.needsOpenChat ?? false) || forced {
            (UIApplication.shared.delegate as? AppDelegate)?.needsOpenChat = false
            
            self.selectedIndex = 0
            
            ((self.viewControllers?.first as? UINavigationController)?.viewControllers.first as? PasswordViewController)?.needsOpenChat = true
            (self.viewControllers?.first as? PasswordViewController)?.needsOpenChat = true

            ((self.viewControllers?.first as? UINavigationController)?.viewControllers.first as? AppendedCardViewController)?.needsOpenChat = true
            (self.viewControllers?.first as? AppendedCardViewController)?.needsOpenChat = true
            
            ((self.viewControllers?.first as? UINavigationController)?.viewControllers.first as? CardViewController)?.needsOpenChat = true
            (self.viewControllers?.first as? CardViewController)?.needsOpenChat = true
            
        }
    }
    
}
