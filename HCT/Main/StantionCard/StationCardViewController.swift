//
//  StantionCardViewController.swift
//  HCT
//
//  Created by Константин on 24.10.2017.
//  Copyright © 2017 Константин. All rights reserved.
//

import UIKit

class StationCardViewController: UIViewController {
    
    class func push(with station: Station?, needsHideNavBar: Bool = true, needsNavBar: Bool = true, from: UINavigationController?, animated: Bool=true) {
        guard station != nil else { return }
        let new = UIStoryboard(name: "StationCard", bundle: nil).instantiateInitialViewController() as! StationCardViewController
        new.station = station
        new.needsNavBar = needsNavBar
        new.needsHideNavBar = needsHideNavBar
        from?.pushViewController(new, animated: animated)
        
    }
    
    @IBOutlet weak var routeButton: UIButton?
    @IBOutlet weak var addressLabel: UILabel?
    @IBOutlet weak var favoriteButton: UIButton?
    @IBOutlet weak var distanceLabel: UILabel?
    @IBOutlet weak var stationTitle: UILabel?
    
    var station: Station!  {
        didSet {
            if isViewLoaded {
                fill()
            }
        }
    }
    
    var needsHideNavBar: Bool!
    var needsNavBar = true
    
    @IBOutlet weak var fuelLabel: UILabel!
    @IBOutlet weak var fuelHeight: NSLayoutConstraint!
    @IBOutlet weak var fuelCollection: UICollectionView! {
        didSet {
            fuelCollection.delegate = self
            fuelCollection.dataSource = self
        }
    }
    
    @IBOutlet weak var servicesLabel: UILabel!
    @IBOutlet weak var servicesHeight: NSLayoutConstraint!
    @IBOutlet weak var servicesCollection: UICollectionView! {
        didSet {
            servicesCollection.delegate = self
            servicesCollection.dataSource = self
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        if needsNavBar {
            self.navigationController?.setNavigationBarHidden(false, animated: true)
        }
        
        fill()
    }

    
    private  func fill() {
        asyncAfter {
            guard self.station != nil else { return }
            
            self.stationTitle?.text = self.station.title
            self.addressLabel?.text = self.station.addr
            self.distanceLabel?.text = self.station.distanceLabel
            self.fuelLabel.text = self.station.fuels.count == 0 ? "Доступные виды топлива не указаны" : "Топливо"
            self.servicesLabel.text = self.station.services.count == 0 ? "Доступные виды услуг не указаны" : "Услуги"
            
            self.fuelCollection?.reloadData()
            self.servicesCollection?.reloadData()
            
            asyncAfter {
                self.fuelHeight.constant = self.station.fuels.count == 0 ? 0 : self.fuelCollection.contentSize.height
                self.servicesHeight.constant = self.station.services.count == 0 ? 0 : self.servicesCollection.contentSize.height
                self.updateFavoritesButton()
                self.fuelCollection?.reloadData()
                self.servicesCollection?.reloadData()
            }
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if needsHideNavBar {
            self.navigationController?.setNavigationBarHidden(true, animated: true)
        }
    }

    @IBAction func showRoute(_ sender: UIButton) {
        RouteViewController.push(with: self.station, needsShowNavBar: true, from: self.navigationController)
    }
    
    @IBAction func favoriteTapped(_ sender: UIButton) {
        station.isFavorite = !station.isFavorite
        CoreDataService.instance.save()
        updateFavoritesButton()
    }
    
    func updateFavoritesButton() {
        self.favoriteButton?.setImage(station.isFavorite ? #imageLiteral(resourceName: "favorite_station") : #imageLiteral(resourceName: "no_favorite_station"), for: .normal)
    }
}

extension StationCardViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == fuelCollection {
            return station?.fuels.count ?? 0
        } else {
            return station?.services.count ?? 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == fuelCollection {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "fuelCell", for: indexPath)
            (cell.viewWithTag(1) as? UILabel)?.text = station.fuels[indexPath.item].title
            (cell.viewWithTag(2) as? UILabel)?.text = "\(station.fuels[indexPath.item].price) ₽"
            return cell
        } else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "serviceCell", for: indexPath) as! ServiceCollectionViewCell
            cell.title.text = station.services[indexPath.item].title
            cell.icon.image = UserDataManager.allServices.first(where: {$0.icon == station.services[indexPath.item].icon})?.image
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == fuelCollection {
            return CGSize(width: (self.view.bounds.width-32)/3 - 16, height: 43)
        } else {
            return CGSize(width: self.view.bounds.width/3, height: 120)
        }
    }
}


