//
//  MapStationInfo.swift
//  HCT
//
//  Created by Константин on 23.10.2017.
//  Copyright © 2017 Константин. All rights reserved.
//

import UIKit
import GoogleMaps

class MapStationInfo: ShadowedView {
    
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var address: UILabel!
    @IBOutlet weak var distance: UILabel!
    @IBOutlet weak var collectionItems: UICollectionView!
    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var collectionHeight: NSLayoutConstraint!
    @IBOutlet weak var fullHeight: NSLayoutConstraint!

    
    var station: Station!
    private var marker: GMSMarker?
    private var tap: UITapGestureRecognizer!
    private var swipe: UISwipeGestureRecognizer!
    private var topSwipe: UISwipeGestureRecognizer!
    private var tappedBlock: (()->Void)!
    
    var isShowned = false
    
    func update(with m: GMSMarker?, tappedBlock block: @escaping ()->Void) {
        tappedBlock = block
        if tap == nil {
            tap = UITapGestureRecognizer(target: self, action: #selector(self.tapped))
            tap.numberOfTapsRequired = 1
            tap.numberOfTouchesRequired = 1
            tap.delaysTouchesBegan = true
            tap.delaysTouchesEnded = false
            self.addGestureRecognizer(tap)
        }
//        if swipe == nil {
//            swipe = UISwipeGestureRecognizer(target: self, action: #selector(self.swiped))
//            swipe.direction = .down
//            self.addGestureRecognizer(swipe)
//
//        }
//
//        if topSwipe == nil {
//            topSwipe = UISwipeGestureRecognizer(target: self, action: #selector(self.swipedUp))
//            topSwipe.direction = .up
//            self.addGestureRecognizer(topSwipe)
//        }
        marker = m
        let s = marker?.userData as? Station
        station = s
        title.text = s?.title
        address.text = s?.addr
        distance.text = s?.distanceLabel
        
        let fullWidth = CGFloat(station?.fuels.count ?? 0)*60
        let numberOfRows = Int((fullWidth/self.collectionItems.bounds.width).rounded(.up))
        collectionHeight.constant = CGFloat(numberOfRows)*21 + CGFloat(numberOfRows - 1)*10
        fullHeight.constant = max(140, 87 + self.title.intrinsicContentSize.height + self.address.intrinsicContentSize.height + collectionHeight.constant - 21)
        
        collectionItems.delegate = self
        collectionItems.dataSource = self
        collectionItems.reloadData()

    }
    
    @objc private func tapped() {
        self.tappedBlock?()
    }
    
    @objc private func swiped() {
        self.hide{}
    }
    
    @objc private func swipedUp() {
        self.tapped()
    }
    
    func show() {
        isShowned = true
        asyncAfter {
            self.bottomConstraint.constant = 0
            UIView.animate(withDuration: 0.4, delay: 0, usingSpringWithDamping: 0.9, initialSpringVelocity: 0.2, options: [], animations: {
                self.superview?.setNeedsLayout()
                self.superview?.layoutIfNeeded()
            }, completion: nil)
        }
    }
    
    
    func hide(_ completion: @escaping ()->Void) {
        isShowned = false
        asyncAfter {
            self.bottomConstraint.constant = 240
            UIView.animate(withDuration: 0.4, delay: 0, usingSpringWithDamping: 0.7, initialSpringVelocity: 0.2, options: [], animations: {
                self.superview?.setNeedsLayout()
                self.superview?.layoutIfNeeded()
            }, completion: {_ in
                completion()
            })
            
        }
        
    }
    
    
    
}


extension MapStationInfo: UICollectionViewDelegate, UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return station?.fuels.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionItems.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath)
        (cell.viewWithTag(1) as? UILabel)?.text = station.fuels[indexPath.item].title
        return cell
    }
    
    
    
}
