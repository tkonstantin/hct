//
//  StationsListViewController.swift
//  HCT
//
//  Created by Константин on 30.10.2017.
//  Copyright © 2017 Константин. All rights reserved.
//

import UIKit
import CoreData

class StationsListViewController: UIViewController, NSFetchedResultsControllerDelegate {
    
    @IBOutlet weak var tableItems: UITableView!
    @IBOutlet weak var filtersButton: UIButton!
    @IBOutlet weak var activity: UIActivityIndicatorView!
    
    var sectionedStations: [[Station]] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //        self.createFetchController()
        self.tableItems.delegate = self
        self.tableItems.dataSource = self
        self.tableItems.tableFooterView = UIView()
    }
    
    
    func fillStations() {
        var indexedStations: [Int64: [Station]] = [:]
        
        var stations = Server.makeRequest.all_stations
        stations.applyFilters()
        stations = stations.filter({$0.distanceCached != 0 })
        for station in stations {
            let key = station.distanceCachedRounded
            if indexedStations.keys.contains(key) {
                indexedStations[key]?.append(station)
            } else {
                indexedStations[key]  = [station]
            }
        }
        sectionedStations = Array(indexedStations.values).sorted(by: {$0.first!.distanceCachedRounded < $1.first!.distanceCachedRounded})
    }
    
    @IBAction func showFilters(_ sender: UIButton) {
    }
    
    
    
    
    //    var fetchController: NSFetchedResultsController<Station>!
    
    //    func createFetchController() {
    
    //        let req: NSFetchRequest<Station> = NSFetchRequest(entityName: "Station")
    //        req.sortDescriptors = [NSSortDescriptor(key: #keyPath(Station.distanceCachedRounded), ascending: true)]
    //
    //        let stations = Server.makeRequest.all_stations
    //        stations.applyFilters()
    //        req.predicate = NSPredicate(block: { (evaluatedObject, bindings) -> Bool in
    //            guard let station = evaluatedObject as? Station else { return false}
    //            guard station.distanceCached != 0 else { return false }
    //            guard FilterDataManager.current.isFiltersActive else { return true}
    //            let fuels = FilterDataManager.current.settings.fuels
    //            let services = FilterDataManager.current.settings.services
    //
    //            var fuelsOK = true
    //            var servicesOK = true
    //
    //            for fuel in fuels {
    //                if !station.fuels.contains(where: {$0.title == fuel.title}) {
    //                    fuelsOK = false
    //                }
    //            }
    //
    //            for service in services {
    //                if !station.services.contains(where: {$0.icon == service.icon}) {
    //                    servicesOK = false
    //                }
    //            }
    //
    //            return fuelsOK && servicesOK
    //        })
    //
    //        fetchController = NSFetchedResultsController(fetchRequest: req, managedObjectContext: CoreDataService.instance.privateContext, sectionNameKeyPath: #keyPath(Station.distanceCachedRounded), cacheName: nil)
    //        fetchController.delegate = self
    
    
    //    }
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        self.filtersButton.setImage(FilterDataManager.current.isFiltersActive ? #imageLiteral(resourceName: "map_filters_button_active") : #imageLiteral(resourceName: "map_filters_button"), for: .normal)
        
        if FilterDataManager.current.needsApplyForList || sectionedStations.count == 0 {
            FilterDataManager.current.needsApplyForList = false
            self.activity.startAnimating()
            UserDataManager.current.calculateDistances {
                CoreDataService.instance.save()
                DispatchQueue.global().async {
                    self.fillStations()
                    asyncAfter {
                        self.tableItems.reloadData()
                        self.activity.stopAnimating()
                    }
                }
                
            }
        }
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    //
    //    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
    //        tableItems.beginUpdates()
    //    }
    //    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
    //        switch type {
    //        case .insert:
    //            tableItems.insertRows(at: [newIndexPath!], with: .none)
    //        case .delete:
    //            tableItems.deleteRows(at: [indexPath!], with: .none)
    //        case .update:
    //            tableItems.reloadRows(at: [indexPath!], with: .none);
    //        case .move:
    //            tableItems.deleteRows(at: [indexPath!], with: .none);
    //            tableItems.insertRows(at: [newIndexPath!], with: .none);
    //        }
    //    }
    //
    //    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange sectionInfo: NSFetchedResultsSectionInfo, atSectionIndex sectionIndex: Int, for type: NSFetchedResultsChangeType) {
    //        switch type {
    //        case .delete:
    //            self.tableItems.deleteSections([sectionIndex], with: .none);
    //        case .insert:
    //            self.tableItems.insertSections([sectionIndex], with: .none)
    //
    //        default:
    //            print("update sectininfo какой то непонятный");
    //        }
    //    }
    //
    //    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
    //        tableItems.endUpdates()
    //    }
    
    
}

extension StationsListViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        //        return fetchController.sections?.count ?? 0
        return sectionedStations.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //        return fetchController.sections?[section].numberOfObjects ?? 0
        return sectionedStations[section].count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //        let station = fetchController.object(at: indexPath)
        let station = sectionedStations[indexPath.section][indexPath.row]
        let cell = tableItems.dequeueReusableCell(withIdentifier: "cell") as! StationTableViewCell
        cell.update(with: station)
        cell.route_button.addTarget(self, action: #selector(self.showRoute(_:)), for: .touchUpInside)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 44
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = UIView(frame: CGRect(origin: .zero, size: CGSize(width: self.view.bounds.width, height: 44)))
        header.backgroundColor = .clear
        let label = UILabel(frame: CGRect(x: 16, y: 20, width: self.view.bounds.width, height: 24))
        label.textColor = darkGrayColor
        label.textAlignment  = .left
        label.font = UIFont.systemFont(ofSize: 12)
        label.text = String(sectionedStations[section].first!.distanceCachedRounded)  + " км"
        //        label.text = (fetchController.sections?[section].name ?? "") + " км"
        header.addSubview(label)
        return header
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return .leastNonzeroMagnitude
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //        let currentStation = fetchController.object(at: indexPath)
        let currentStation = sectionedStations[indexPath.section][indexPath.row]
        StationCardViewController.push(with: currentStation, needsHideNavBar: false, from: self.navigationController)
    }
    
    @objc private func showRoute(_ sender: RouteButton) {
        RouteViewController.push(with: sender.station, needsShowNavBar: true, from: self.navigationController)
    }
    
}
