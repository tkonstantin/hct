//
//  FavoritesViewController.swift
//  HCT
//
//  Created by Константин on 23.10.2017.
//  Copyright © 2017 Константин. All rights reserved.
//

import UIKit

class FavoritesViewController: UIViewController {

    @IBOutlet weak var tableItems: UITableView!
    
    var stations: [Station] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableItems.delegate = self
        tableItems.dataSource = self
        tableItems.tableFooterView = UIView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        updateFavorites()
    }
    
    func updateFavorites() {
        stations = Server.makeRequest.all_stations.filter({$0.isFavorite})
        self.tableItems.reloadData()
    }
    
    @IBAction func changeEditingState(_ sender: UIButton) {
        if tableItems.isEditing {
            sender.setTitle("ИЗМЕНИТЬ", for: .normal)
            tableItems.setEditing(false, animated: true)
        } else {
            sender.setTitle("ГОТОВО", for: .normal)
            tableItems.setEditing(true, animated: true)
        }
    }
    
}


extension FavoritesViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return stations.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! StationTableViewCell
        cell.update(with: stations[indexPath.section])
        cell.route_button.addTarget(self, action: #selector(self.showRoute(_:)), for: .touchUpInside)
        return cell
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            let currentStation = Server.makeRequest.all_stations.filter({$0.isFavorite})[indexPath.row]
            currentStation.isFavorite = false
            CoreDataService.instance.save()
            stations = Server.makeRequest.all_stations.filter({$0.isFavorite})
            self.tableItems.deleteSections([indexPath.section], with: .automatic)
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        StationCardViewController.push(with: stations[indexPath.section], needsHideNavBar: false, from: self.navigationController)
    }
    
    @objc private func showRoute(_ sender: RouteButton) {
        RouteViewController.push(with: sender.station, needsShowNavBar: true, from: self.navigationController)
    }
}
