//
//  StationTableViewCell.swift
//  HCT
//
//  Created by Константин on 24.10.2017.
//  Copyright © 2017 Константин. All rights reserved.
//

import UIKit

class StationTableViewCell: UITableViewCell {

    @IBOutlet weak var route_button: RouteButton!
    @IBOutlet weak var fuels: UILabel!
    @IBOutlet weak var address: UILabel!
    @IBOutlet weak var title: UILabel!
    
    func update(with station: Station) {
        address.text = station.addr
        title.text = station.title
        fuels.text = station.fuels.map({$0.title}).joined(separator: "  ")
        route_button.station = station

    }
}
