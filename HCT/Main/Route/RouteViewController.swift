//
//  RouteViewController.swift
//  HCT
//
//  Created by Константин on 30.10.2017.
//  Copyright © 2017 Константин. All rights reserved.
//

import UIKit

class RouteViewController: UIViewController {
    
    class func push(with station: Station?, needsShowNavBar: Bool = true, from: UINavigationController?) {
        guard station != nil else { return }
        let new = UIStoryboard(name: "Route", bundle: nil).instantiateInitialViewController() as! RouteViewController
        new.station = station
        new.needsShowNavBar = needsShowNavBar
        
        if let location = UserDataManager.locationManager.location  {
            new.firstCoordinate = location.coordinate
        } else {
            new.firstCoordinate = station!.position
        }
        new.lastCoordinate = station!.position
        
        from?.pushViewController(new, animated: true)
    }
    
    var station: Station!
    var needsShowNavBar = true
    
    private var routePolyline: GMSPolyline!

    
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var durationLabel: UILabel!
    @IBOutlet weak var distanceLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    
    @IBOutlet weak var my_locationField: UITextField! {
        didSet {
            my_locationField.attributedPlaceholder = NSAttributedString(string: "Мое местоположение", attributes: [NSAttributedStringKey.foregroundColor : UIColor(hex: "40453F"), NSAttributedStringKey.font: UIFont.systemFont(ofSize: 13)])
        }
    }
    
    
    var firstCoordinate: CLLocationCoordinate2D!
    var lastCoordinate: CLLocationCoordinate2D!
    
    @IBOutlet weak var autocompletableField: AutocompletableTextField! {
        didSet {
            guard let coordinate = UserDataManager.locationManager.location?.coordinate else { return }
            let topCoordtinate = CLLocationCoordinate2D(latitude: coordinate.latitude + 0.5, longitude: coordinate.longitude + 0.5)
            let bottomCoordinate = CLLocationCoordinate2D(latitude: coordinate.latitude - 0.5, longitude: coordinate.longitude - 0.5)
            let targetBounds = GMSCoordinateBounds(coordinate: topCoordtinate, coordinate: bottomCoordinate)
            autocompletableField.configure(delegate: self, autocompleteBounds: targetBounds)
        }
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if !needsShowNavBar {
            self.navigationController?.setNavigationBarHidden(true, animated: true)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configMap()
        addressLabel.text = station.addr
    }
    
    @IBAction func plus(_ sender: UIButton) {
        self.mapView.animate(toZoom: self.mapView.camera.zoom + 0.5)
    }
    
    @IBAction func minus(_ sender: UIButton) {
        self.mapView.animate(toZoom: self.mapView.camera.zoom - 0.5)
        
    }
    
    
    @IBAction func updateRoute(_ sender: UIButton) {
        self.getPolylineRoute(from: firstCoordinate, to: lastCoordinate)
    }
    
    
    @IBAction func myLoc(_ sender: UIButton) {
        if let coord = self.mapView.myLocation?.coordinate {
            self.mapView.animate(toLocation: coord)
        }
    }
    
    func updateLabels(distance: String?, duration: String?) {
        asyncAfter {
            self.durationLabel.text = duration
            if distance != nil {
                self.distanceLabel.text = "(" + distance! + ")"
            }
        }
    }
    
    @IBAction func shouldRedirect(_ sender: UIButton) {
        guard let source = firstCoordinate, let destination = lastCoordinate else { return }
        
        let googleDeeplink = URL(string: "comgooglemaps://?saddr=\(source.latitude),\(source.longitude)&daddr=\(destination.latitude),\(destination.longitude)&directionsmode=driving")
        if googleDeeplink != nil && UIApplication.shared.canOpenURL(URL(string:"comgooglemaps://")!) {
            UIApplication.shared.openURL(googleDeeplink!)
        } else {
            let appleDeeplink = URL(string: "http://maps.apple.com/?saddr=\(source.latitude),\(source.longitude)&daddr=\(destination.latitude),\(destination.longitude)&dirflg=d")!
            UIApplication.shared.openURL(appleDeeplink)
        }
    }
    
    
}

extension RouteViewController: AutocompletableTextFieldDelegate {
    func autocomplete_didSelect(coordinate: CLLocationCoordinate2D?) {
        if let coord = coordinate {
            firstCoordinate = coord
            self.updateRoute(UIButton())
            self.view.endEditing(true)
        }
    }
}


extension RouteViewController { //Map
    func configMap() {
        
        let marker = GMSMarker(position: station.position)
        marker.icon = #imageLiteral(resourceName: "map_stantion_active")
        marker.map = mapView
        
        mapView.isMyLocationEnabled = true
        if let userCoordinate = firstCoordinate {
            mapView.animate(toLocation: userCoordinate)
            mapView.animate(toZoom: 10)
            asyncAfter(milliseconds: 300, {
                UIView.animate(withDuration: 0.3, animations: {
                    self.mapView.alpha = 1
                    self.getPolylineRoute(from: userCoordinate, to: self.station.position)
                })
            })
            
        }
    }
    
    
    func getPolylineRoute(from source: CLLocationCoordinate2D, to destination: CLLocationCoordinate2D){
        
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        let googleDirectionsApiKey = "AIzaSyAYL77D01zbpt7U9CjtAKGHTVgWaACCZhU"
        
        let url = URL(string: "https://maps.googleapis.com/maps/api/directions/json?origin=\(source.latitude),\(source.longitude)&destination=\(destination.latitude),\(destination.longitude)&sensor=true&mode=driving&key=\(googleDirectionsApiKey)")!
        
        let task = session.dataTask(with: url, completionHandler: {
            (data, response, error) in
            if error != nil {
                print(error!.localizedDescription)
                self.showRouteError()
            }
            else {
                do {
                    if let json:[String: Any] = try JSONSerialization.jsonObject(with: data!, options: .allowFragments) as? [String: Any] {
                        guard let routes = json["routes"] as? NSArray else {
                            self.showRouteError()
                            return
                        }
                        
                        guard (routes.count > 0) else { self.showRouteError(); return; }
                            let overview_polyline = routes[0] as? NSDictionary
                        if let leg = (overview_polyline?["legs"] as? [NSDictionary])?.first {
                            let distanceString = (leg["distance"] as? NSDictionary)?["text"] as? String
                            let durationString = (leg["duration"] as? NSDictionary)?["text"] as? String
                            self.updateLabels(distance: distanceString, duration: durationString)
                        }
                            let dictPolyline = overview_polyline?["overview_polyline"] as? NSDictionary
                            
                            let points = dictPolyline?.object(forKey: "points") as? String
                            
                            self.showPath(polyStr: points!)
                            
                            DispatchQueue.main.async {
                                let bounds = GMSCoordinateBounds(coordinate: source, coordinate: destination)
                                let update = GMSCameraUpdate.fit(bounds, with: UIEdgeInsetsMake(170, 60, 60, 60))
                                self.mapView!.moveCamera(update)
                            }
                    } else {
                        self.showRouteError()
                    }
                } catch {
                    self.showRouteError()
                }
            }
        })
        task.resume()
    }
    
    private func showRouteError() {
        Presenter.instance.showErrorAlert("Не удалось построить маршрут", message: "Попробуйте позже")

    }
    
    
    func showPath(polyStr :String){
        let path = GMSPath(fromEncodedPath: polyStr)
        routePolyline?.strokeColor = .clear
        routePolyline = GMSPolyline(path: path)
        routePolyline.strokeWidth = 3.0
        routePolyline.strokeColor = UIColor(hex: "00A8E1")
        routePolyline.map = mapView
    }
}
