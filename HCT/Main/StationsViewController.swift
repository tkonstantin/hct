//
//  StantionsViewController.swift
//  HCT
//
//  Created by Константин on 23.10.2017.
//  Copyright © 2017 Константин. All rights reserved.
//

import UIKit
import GoogleMaps
import CoreLocation
import GooglePlaces

class StationsViewController: UIViewController {
    
    var clusterManager: GMUClusterManager!
    
    @IBOutlet weak var activity: UIActivityIndicatorView!
    @IBOutlet weak var infoViewAlightSafeAreaConstraint: NSLayoutConstraint!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var statusMask: UIView!
    @IBOutlet weak var favoritesButton: UIButton!
    @IBOutlet weak var routeButton: UIButton!
    @IBOutlet weak var routeView: ShadowedView!
    @IBOutlet weak var filtersButton: UIImageView!
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var stationInfo: MapStationInfo!
    @IBOutlet weak var autocompletableField: AutocompletableTextField! {
        didSet {
            guard let coordinate = UserDataManager.locationManager.location?.coordinate else { return }
            let topCoordtinate = CLLocationCoordinate2D(latitude: coordinate.latitude + 0.5, longitude: coordinate.longitude + 0.5)
            let bottomCoordinate = CLLocationCoordinate2D(latitude: coordinate.latitude - 0.5, longitude: coordinate.longitude - 0.5)
            let targetBounds = GMSCoordinateBounds(coordinate: topCoordtinate, coordinate: bottomCoordinate)
            autocompletableField.configure(delegate: self, autocompleteBounds: targetBounds, stationInfo: stationInfo)
        }
    }
    
    var stations: [Station] = []
    
    var selectedStation: Station?
    
    var isTrackingStationCardNow = false
    var activeMarker: GMSMarker!
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.filtersButton.image = FilterDataManager.current.isFiltersActive ? #imageLiteral(resourceName: "map_filters_button_active") : #imageLiteral(resourceName: "map_filters_button")
        self.favoritesButton?.setImage((selectedStation?.isFavorite ?? false) ? #imageLiteral(resourceName: "favorite_station") : #imageLiteral(resourceName: "no_favorite_station"), for: .normal)
        
        asyncAfter {
            self.loadStations()
        }
        
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.subscribeToKeyboard()
        
        self.addTapOutsideGestureRecognizer()
        
        UserDataManager.locationManager.delegate = self
        
        asyncAfter(milliseconds: 300) {
            if CLLocationManager.authorizationStatus() == .authorizedWhenInUse {
                self.startUpdates()
            } else {
                UserDataManager.locationManager.requestWhenInUseAuthorization()
            }
            
            self.loadStations()
        }
        
    }
    
    @IBAction func favoritesTapped(_ sender: UIButton) {
        selectedStation?.isFavorite = !selectedStation!.isFavorite
        CoreDataService.instance.save()
        sender.setImage((selectedStation?.isFavorite ?? false) ? #imageLiteral(resourceName: "favorite_station") : #imageLiteral(resourceName: "no_favorite_station"), for: .normal)
    }
    
    
    
    override func keyboardWillBeShown(_ notification: Notification) {
        activeMarker?.icon = #imageLiteral(resourceName: "map_stantion_inactive")
        self.stationInfo.hide{}
    }
    
    
    func loadStations() {
        guard FilterDataManager.current.needsApplyForMap else { return }
        FilterDataManager.current.needsApplyForMap = false
        activity.startAnimating()
        DispatchQueue.global().async {
            self.stations = Server.makeRequest.all_stations
            self.stations.applyFilters()
            asyncAfter {
                self.loadClustering()
                self.activity.stopAnimating()
            }
        }
        
    }
    
    
    func startUpdates() {
        mapView.alpha = 0
        UserDataManager.locationManager.pausesLocationUpdatesAutomatically = true
        UserDataManager.locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters
        UserDataManager.locationManager.startUpdatingLocation()
        mapView.isMyLocationEnabled = true
        mapView.delegate = self
        if UserDataManager.locationManager.location != nil {
            mapView.animate(toLocation: UserDataManager.locationManager.location!.coordinate)
            mapView.animate(toZoom: 10)
            asyncAfter(milliseconds: 300, {
                UIView.animate(withDuration: 0.3, animations: {
                    self.mapView.alpha = 1
                })
            })
            
        }
    }
    
    @IBAction func plus(_ sender: UIButton) {
        self.mapView.animate(toZoom: self.mapView.camera.zoom + 0.5)
    }
    
    @IBAction func minus(_ sender: UIButton) {
        self.mapView.animate(toZoom: self.mapView.camera.zoom - 0.5)
        
    }
    
    @IBAction func myLoc(_ sender: UIButton) {
        if let coord = self.mapView.myLocation?.coordinate {
            self.mapView.animate(toLocation: coord)
        }
    }
    
    @IBAction func showRoute(_ sender: UIButton) {
        RouteViewController.push(with: self.selectedStation, needsShowNavBar: false, from: self.navigationController)
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let card = segue.destination as? StationCardViewController {
            card.station = self.selectedStation
            card.needsHideNavBar = true
            card.needsNavBar = false
            card.view.isUserInteractionEnabled = false
        }
    }
    
}

extension StationsViewController: AutocompletableTextFieldDelegate {
    func autocomplete_didSelect(coordinate: CLLocationCoordinate2D?) {
        guard let coord = coordinate else { return }
        DispatchQueue.global().async {
            let loc = CLLocation(latitude: coord.latitude, longitude: coord.longitude)
            let allStations = Server.makeRequest.all_stations
            let allDistances = allStations.map({$0.cllocation.distance(from: loc)})
            if let index = allDistances.index(of: allDistances.sorted(by: {$0 < $1}).first!) {
                let closestStation = allStations[index]
                
                asyncAfter {
                    self.mapView.animate(toLocation: closestStation.position)
                    self.mapView.animate(toZoom: 13)
                    self.view.endEditing(true)
                }
            }
        }

    }
}


extension StationsViewController: GMSMapViewDelegate {
    
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        mapView.animate(toLocation: marker.position)
        guard marker.userData is Station else { return false }
        activeMarker?.icon = #imageLiteral(resourceName: "map_stantion_inactive")
        activeMarker = marker
        if marker.icon == #imageLiteral(resourceName: "map_stantion_active") {
            activeMarker?.icon = #imageLiteral(resourceName: "map_stantion_inactive")
            stationInfo.hide{}
        } else {
            marker.icon = #imageLiteral(resourceName: "map_stantion_active")
            self.selectedStation = marker.userData as? Station
            self.favoritesButton.setImage((selectedStation?.isFavorite ?? false) ? #imageLiteral(resourceName: "favorite_station") : #imageLiteral(resourceName: "no_favorite_station"), for: .normal)
            (self.childViewControllers.first as? StationCardViewController)?.station = self.selectedStation
            self.stationInfo.update(with: marker, tappedBlock: {
                if !self.infoViewAlightSafeAreaConstraint.isActive {
                    StationCardViewController.push(with: self.selectedStation, from: self.navigationController)
                }
            })
            self.stationInfo.show()
        }
        return true
    }
    
    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
        self.view.endEditing(true)
        self.autocompletableField?.updatePredictions([])
        activeMarker?.icon = #imageLiteral(resourceName: "map_stantion_inactive")
        self.stationInfo.hide{}
    }
    
}

extension StationsViewController: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedWhenInUse {
            self.startUpdates()
        }
    }
}


extension StationsViewController: GMUClusterManagerDelegate, GMUClusterRendererDelegate {
    
    func loadClustering() {
        clusterManager?.clearItems()
        
        let iconGenerator = GMUDefaultClusterIconGenerator(buckets: [1000000] as [NSNumber], backgroundImages: [#imageLiteral(resourceName: "map_cluster_backgound")])
        let algo = GMUNonHierarchicalDistanceBasedAlgorithm()
        let renderer = GMUDefaultClusterRenderer(mapView: mapView, clusterIconGenerator: iconGenerator)
        renderer.delegate = self
        renderer.animatesClusters = false
        clusterManager = GMUClusterManager(map: mapView, algorithm: algo, renderer: renderer)
        clusterManager.setDelegate(self, mapDelegate: self)
        
        clusterManager.add(self.stations)
        
        clusterManager.cluster()
    }
    
    
    
    func renderer(_ renderer: GMUClusterRenderer, markerFor object: Any) -> GMSMarker {
        if let station = object as? Station {
            let marker = GMSMarker(position: CLLocationCoordinate2D(latitude: station.lat, longitude: station.lon))
            let isActive = (self.stationInfo.station == station && self.stationInfo.isShowned)
            marker.icon = isActive ? #imageLiteral(resourceName: "map_stantion_active") : #imageLiteral(resourceName: "map_stantion_inactive")
            marker.map = self.mapView
            marker.userData = station
            if isActive {
                activeMarker = marker
            }
            return marker
        } else {
            return GMSMarker()
        }
    }
}

extension StationsViewController {
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        guard let touch = touches.first else { return }
        if self.stationInfo.frame.contains(touch.location(in: self.stationInfo.superview!)) || self.bottomView.frame.contains(touch.location(in: self.bottomView.superview!)) {
            isTrackingStationCardNow = true
        }
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        guard let touch = touches.first else { return }
        if isTrackingStationCardNow {
            let deltaVertical = touch.location(in: self.view).y - touch.previousLocation(in: self.view).y
            self.stationInfo.frame.origin.y += deltaVertical
            self.routeView.frame.origin.y += deltaVertical
            self.bottomView.frame.origin.y += deltaVertical
            if deltaVertical > 0 {
                self.statusMask.alpha = 0
            }
        }
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        guard let touch = touches.first else { return }
        let deltaVertical = touch.location(in: self.view).y - touch.previousLocation(in: self.view).y // < 0 значит вверх
        guard deltaVertical != 0 else { return }
        
        isTrackingStationCardNow = false
        
        let cardNormalOrigin = self.view.bounds.height - self.stationInfo.bounds.height
        
        if self.stationInfo.frame.origin.y > cardNormalOrigin && deltaVertical > 0 {
            activeMarker?.icon = #imageLiteral(resourceName: "map_stantion_inactive")
            self.infoViewAlightSafeAreaConstraint.isActive = false
            self.stationInfo.bottomConstraint.isActive = true
            self.statusMask.alpha = 0
            self.stationInfo.hide {}
        } else {
            if deltaVertical < 0 {
                UIView.animate(withDuration: 0.6, delay: 0, usingSpringWithDamping: 0.7, initialSpringVelocity: 0.2, options: [], animations: {
                    self.stationInfo.frame.origin.y = 20
                    self.bottomView.frame.origin.y = self.stationInfo.frame.origin.y + self.stationInfo.bounds.height
                    self.routeView.frame.origin.y = -8
                    self.routeView.alpha = 0
                    self.favoritesButton.alpha = 1
                    self.statusMask.alpha = 1
                    self.infoViewAlightSafeAreaConstraint.isActive = true
                    self.stationInfo.bottomConstraint.isActive = false
                    self.statusMask.alpha = 1
                }, completion: { _ in
                    //                    StationCardViewController.push(with: self.selectedStation, needsNavBar: false, from: self.navigationController, animated: false)
                    //                    asyncAfter(milliseconds: 300, {
                    //                        self.stationInfo.show() //was hide here
                    //                    })
                })
            } else {
                self.infoViewAlightSafeAreaConstraint.isActive = false
                self.stationInfo.bottomConstraint.isActive = true
                self.stationInfo.show()
                self.routeView.alpha = 1
                self.favoritesButton.alpha = 0
                self.statusMask.alpha = 0
            }
            
        }
    }
    
    override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.infoViewAlightSafeAreaConstraint.isActive = false
        isTrackingStationCardNow = false
        self.stationInfo.setNeedsLayout()
        self.stationInfo.layoutIfNeeded()
        
    }
}


extension Array where Element: Station {
    mutating func applyFilters() {
        guard FilterDataManager.current.isFiltersActive else { return }
        var temp = Array<Element>()
        let fuelTitles = Set(FilterDataManager.current.settings.fuels.map({$0.title}))
        let serviceIcons = Set(FilterDataManager.current.settings.services.map({$0.icon}))
        for station in self {
            if fuelTitles.intersection(Set(station.fuels.map({$0.title}))).count != 0 {
                temp.append(station)
            } else if serviceIcons.intersection(Set(station.services.map({$0.icon}))).count != 0 {
                temp.append(station)
            }
        }
        
        self = temp
    }
    
    
    mutating func appendSafely(newValues: [Element], shouldUpdateExistingValues: Bool = true) {
        for new in newValues {
            if !shouldUpdateExistingValues {
                if !self.contains(where: {$0.id == new.id})  {
                    self.append(new)
                }
            } else {
                if let index = self.index(where: {$0.id == new.id}) {
                    self[index] = new
                } else {
                    self.append(new)
                }
            }
            
        }
    }
}



