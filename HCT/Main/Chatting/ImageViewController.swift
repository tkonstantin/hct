//
//  ImageViewController.swift
//  HCT
//
//  Created by Константин on 07.02.2018.
//  Copyright © 2018 Константин. All rights reserved.
//

import UIKit


protocol ImageControllerDelegate: class {
    func shouldSend(image: UIImage)
}

class ImageViewController: UIViewController, UIScrollViewDelegate {
    
    class func makeOne(with image: UIImage, delegate: ImageControllerDelegate?, needsButton: Bool = false) -> ImageViewController {
        let new = UIStoryboard(name: "Chatting", bundle: nil).instantiateViewController(withIdentifier: "ImageViewController") as! ImageViewController
        new.image = image
        new.needsButton = needsButton
        new.delegate = delegate
        return new
    }
    fileprivate weak var delegate: ImageControllerDelegate?
    fileprivate var needsButton = false
    fileprivate var image: UIImage!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var sendButton: UIButton!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        imgView.image = image
        sendButton.isHidden = !needsButton
        
    }
    
    
    
    override var prefersStatusBarHidden: Bool {
        get {
            return true
        }
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    @IBAction func sendTapped(_ sender: UIButton) {
        delegate?.shouldSend(image: image)
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func backTapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)

    }
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return imgView
    }

}
