//
//  ChatViewController.swift
//  HCT
//
//  Created by Константин on 01.02.2018.
//  Copyright © 2018 Константин. All rights reserved.
//

import Foundation
import UIKit
import SVProgressHUD

let historyPageSize = 100

class ChatViewController: UIViewController {
    
    class func makeOne() -> ChatViewController {
        let new = UIStoryboard(name: "Chatting", bundle: nil).instantiateViewController(withIdentifier: "ChatViewController") as! ChatViewController
        return new
    }
    
    
    var messages: [Message] = []
    
    @IBOutlet weak var table: UITableView!
    @IBOutlet weak var inputBottom: NSLayoutConstraint!
    @IBOutlet weak var input: UIView!
    @IBOutlet weak var inputField: UITextField!
    @IBOutlet weak var activity: UIActivityIndicatorView!
    
    override var shouldAutorotate: Bool {
        get {
            return false
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        subscribeForNewMessages()
        
        table.transform = CGAffineTransform(rotationAngle: .pi)
        self.addTapOutsideGestureRecognizer()
        self.subscribeToKeyboard()
        
        loadMessages()
        Server.makeRequest.receivedMessagesCount = 0
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        (UIApplication.shared.delegate as? AppDelegate)?.needsOpenChat = false
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.view.setNeedsLayout()
        self.view.layoutIfNeeded()
        
        table.scrollIndicatorInsets = UIEdgeInsetsMake(8, 0, 8, view.bounds.size.width-8);

    }
    
    var currentPage: Int = 0
    var hasNext: Bool = true
    var isMakingRequest = false
    
    func loadMessages() {
        messages = sendedImageMessages
        guard !isMakingRequest && hasNext else { return }
        isMakingRequest = true
        Server.makeRequest.getMessagesHistory(page: currentPage) { (result) in
            self.currentPage += 1
            self.hasNext = result.count == historyPageSize
            self.isMakingRequest = false
            asyncAfter {
                self.activity?.stopAnimating()
                self.messages += result
                self.messages.sort(by: {$0.date > $1.date})
                self.table.reloadData()
                
            }
        }
    }
    
    private func subscribeForNewMessages() {
        NotificationCenter.default.addObserver(self, selector: #selector(self.reload(_:)), name: newMessageNotification, object: nil)
    }
    
    @objc private func reload(_ notification:  Notification) {
        guard isViewLoaded else { return }
        Server.makeRequest.receivedMessagesCount = 0
        if let userInfo = notification.userInfo, let messageText = userInfo["body"] as? String, !messageText.isEmpty  {
            let newMessage = Message(createdWith: messageText, isIncoming: true)
            self.messages.append(newMessage)
            self.messages.sort(by: {$0.date > $1.date})
            asyncAfter {
                self.table.reloadData()
            }
        }
    }

    
    override func keyboardWillBeShown(_ notification: Notification) {
        if let size = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.size {
            UIView.animate(withDuration: 0.25) {
                if #available(iOS 11.0, *) {
                    self.inputBottom.constant = -size.height + self.view.safeAreaInsets.bottom
                } else {
                    self.inputBottom.constant = -size.height
                }
                
                self.view.setNeedsLayout()
                self.view.layoutIfNeeded()
            }
        }
        
    }
    
    override func keyboardWillBeHidden() {
        UIView.animate(withDuration: 0.25) {
            self.inputBottom.constant = 0
            self.view.setNeedsLayout()
            self.view.layoutIfNeeded()
        }
    }
    
    @IBAction func docTapped(_ sender: UIButton) {
        self.view.endEditing(true)
        
        let docPicker = UIDocumentPickerViewController(documentTypes: ["public.data"], in: .import)
        docPicker.delegate = self
        self.present(docPicker, animated: true, completion: nil)
        
    }
    
    @IBAction func photoTapped(_ sender: UIButton) {
        self.view.endEditing(true)
        
        let alert = UIAlertController(title: "Выберите источник", message: nil, preferredStyle: .actionSheet)
        
        let picker = UIImagePickerController()
        picker.delegate = self
        alert.addAction(UIAlertAction(title: "Камера", style: .default, handler: { (_) in
            picker.sourceType = .camera
            self.present(picker, animated: true, completion: nil)
        }))
        
        alert.addAction(UIAlertAction(title: "Альбомы", style: .default, handler: { (_) in
            picker.sourceType = .photoLibrary
            self.present(picker, animated: true, completion: nil)
        }))
        
        alert.addAction(UIAlertAction(title: "Все фото", style: .default, handler: { (_) in
            picker.sourceType = .savedPhotosAlbum
            self.present(picker, animated: true, completion: nil)
        }))
        
        alert.addAction(UIAlertAction(title: "Отменить", style: .cancel, handler: { (_) in
        }))
        
        self.present(alert, animated: true, completion: nil)
        
    }
    
    
    @IBAction func sendTapped(_ sender: UIButton) {
        self.view.endEditing(true)
        sendMessage()
    }
    
    func sendMessage() {
        guard let text = inputField.text?.trimmingCharacters(in: .whitespacesAndNewlines), !text.isEmpty else { return }
        SVProgressHUD.show()
        inputField.text = nil
        Server.makeRequest.sendFeeedback(text) { (success, errorMessage) in
            if success {
                let newMessage = Message(createdWith: text)
                self.messages.append(newMessage)
                self.messages.sort(by: {$0.date > $1.date})
                asyncAfter {
                    SVProgressHUD.dismiss()
                    self.table.reloadData()
                }
            } else {
                SVProgressHUD.dismiss()
                Presenter.instance.showErrorAlert("Ошибка", message: errorMessage ?? "Неизвестная ошибка")
            }
        }
    }
    
}

extension ChatViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIDocumentPickerDelegate, ImageControllerDelegate {
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        picker.dismiss(animated: true, completion: nil)
        if let img = (info[UIImagePickerControllerOriginalImage] as? UIImage) {
            let vc = ImageViewController.makeOne(with: img, delegate: self, needsButton: true)
            self.navigationController?.pushViewController(vc, animated: true)
            
        }
    }
    
    func shouldSend(image: UIImage) {
        SVProgressHUD.show(withStatus: "Загрузка...")
        Server.makeRequest.sendFile(image, completion: { (success) in
            if success {
                let newMessage = Message(createdWith: image)
                sendedImageMessages = sendedImageMessages + [newMessage]
                self.messages.append(newMessage)
                self.messages.sort(by: {$0.date > $1.date})
                asyncAfter {
                    SVProgressHUD.dismiss()
                    self.table.reloadData()
                }
            } else {
                Presenter.instance.showErrorAlert("Ошибка загрузки", message: "Не удалось загрузить файл. Попробуйте позже или обратитесь в службу поддержки")
            }
        })
    }
    
    func documentPickerWasCancelled(_ controller: UIDocumentPickerViewController) {
        controller.dismiss(animated: true, completion: nil)
        
    }
    
    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentAt url: URL) {
        controller.dismiss(animated: true, completion: nil)
    }
    
    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentsAt urls: [URL]) {
        controller.dismiss(animated: true, completion: nil)
    }
    
    
}


extension ChatViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return messages.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let current = messages[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: current.incoming ? "incoming" : "outgoing", for: indexPath) as! MessageCollectionViewCell
        cell.update(current)
        cell.contentView.transform = CGAffineTransform(rotationAngle: .pi)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let current = messages[indexPath.row]
        if let img = current.image_local {
            self.navigationController?.pushViewController(ImageViewController.makeOne(with: img, delegate: nil), animated: true)
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == (currentPage)*historyPageSize - 3 {
            self.loadMessages()
        }
    }
    
    func scrollViewShouldScrollToTop(_ scrollView: UIScrollView) -> Bool {
        table.scrollToRow(at: IndexPath(row: table.numberOfRows(inSection: 0) - 1, section: 0), at: .top, animated: true)
        return false
    }
    
}


extension ChatViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        asyncAfter {
            textField.resignFirstResponder()
            self.sendMessage()
        }
        return true
    }
    
}

