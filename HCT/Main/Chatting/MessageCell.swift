//
//  MessageCell.swift
//  HCT
//
//  Created by Константин on 01.02.2018.
//  Copyright © 2018 Константин. All rights reserved.
//

import UIKit

class MessageCollectionViewCell: UITableViewCell {
    
    @IBOutlet weak var txt: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var img: UIImageView!

    func update(_ message: Message) {
        dateLabel.text = message.date.parseDateShort(dots: true) + " " + message.date.parseTime()
        txt.text = message.text
        let imageExists = message.image_local != nil || URL(string: message.image ?? "") != nil
        img.image = imageExists ? #imageLiteral(resourceName: "attach_photo").withRenderingMode(.alwaysTemplate).imageWithColor(.white) : nil
        
    }
}
