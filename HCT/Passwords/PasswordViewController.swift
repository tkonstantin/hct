//
//  PasswordViewController.swift
//  HCT
//
//  Created by Константин on 01.11.2017.
//  Copyright © 2017 Константин. All rights reserved.
//

import UIKit

fileprivate class PasswordsManager {
    static let instance = PasswordsManager()
    
    private let currentPasswordkey: NSString = "cpudk"
    
    var currentPassword: String? {
        get {
            let res = String(data: KeychainService.loadData(for: currentPasswordkey) as Data? ?? Data(), encoding: String.Encoding.utf8)
            return res
        } set {
            KeychainService.save(data: newValue?.data(using: String.Encoding.utf8) as NSData? ?? NSData(), for: currentPasswordkey)
        }
    }
    
    var creatingPassword: String?
    
    
}

class PasswordViewController: UIViewController {
    
    class func makeOne(in mode: PasswordViewController.modes = PasswordViewController.modes.create) -> (UINavigationController, PasswordViewController?) {
        let new = UIStoryboard(name: "Passwords", bundle: nil).instantiateInitialViewController() as! UINavigationController
        (new.viewControllers.first as? PasswordViewController)?.mode = mode
        return (new, new.viewControllers.first as? PasswordViewController)
    }
    
    
    enum modes {
        case create
        case `repeat`
        case change
        case enter
    }
    
    var mode: modes = .create
    var needsOpenChat = false
    
    @IBOutlet weak var stack: UIStackView!
    @IBOutlet weak var field: CardNumberField!
    
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var subtitleLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var shadowedView: ShadowedView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addTapOutsideGestureRecognizer()
        field.becomeFirstResponder()
        field.delegate = self
        
        if mode == .enter {
            field.onResetPasswordTapped(block: {
                UserDataManager.current.reset()
                self.tabBarController?.viewControllers?[0] = CardViewController.createOne()
            })
        }
    }
    @IBAction func cancelTapped(_ sender: UIButton) {
        self.view.endEditing(true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        configLabels()
    }
    
    func configLabels() {
        switch mode {
        case .create:
            titleLabel?.text = "Создайте ваш персональный пароль для доступа к приложению"
            subtitleLabel?.text = "Введите цифровой код из 4 цифр"
        case .repeat:
            titleLabel?.text = "Создайте ваш персональный пароль для доступа к приложению"
            subtitleLabel?.text = "Повторите код"
        case .change:
            titleLabel?.text = "Необходимо ввести текущий пароль, чтобы изменить его"
            subtitleLabel?.text = ""
        case .enter:
            titleLabel?.text = "Введите пароль для доступа к информации  о вашей карте"
            subtitleLabel?.text = ""
            
            self.view.backgroundColor = .white
            self.shadowedView.layer.shadowColor = UIColor.clear.cgColor
            
        }
    }
    
    
    func updateDots() {
        for dot in (stack.subviews.filter({$0 is UIImageView}) as! [UIImageView]) {
            dot.image = dot.tag <= field.currentNumber.count ? #imageLiteral(resourceName: "code_dot_filled") : #imageLiteral(resourceName: "code_dot_empty")
        }
        if field.currentNumber.count == 4 {
            goNext()
        }
    }
    
    func goNext() {
        var enteredPassword = ""
        field.currentNumber.map({String($0)}).forEach({enteredPassword += $0})
        
        switch mode {
        case .create:
            PasswordsManager.instance.creatingPassword = enteredPassword
            let new = PasswordViewController.makeOne(in: .repeat)
            new.1?.needsOpenChat = self.needsOpenChat
            self.needsOpenChat = false
            self.tabBarController?.viewControllers?[0] = new.0
        case .repeat:
            
            if PasswordsManager.instance.creatingPassword == enteredPassword {
                PasswordsManager.instance.currentPassword = enteredPassword
                let new = AppendedCardViewController.createOne()
                new.1?.needsOpenChat = self.needsOpenChat
                self.needsOpenChat = false
                self.tabBarController?.viewControllers?[0] = new.0
            } else {
                
                //пароли не совпали
                clearField()
                
                subtitleLabel?.text = "Пароли не совпали"
                asyncAfter(milliseconds: 1000, {
                    let new = PasswordViewController.makeOne()
                    new.1?.needsOpenChat = self.needsOpenChat
                    self.needsOpenChat = false
                    self.tabBarController?.viewControllers?[0] = new.0
                })
            }
        case .change:
            if PasswordsManager.instance.currentPassword == enteredPassword {
                let new = PasswordViewController.makeOne()
                new.1?.needsOpenChat = self.needsOpenChat
                self.needsOpenChat = false
                self.tabBarController?.viewControllers?[0] = new.0
            } else {
                
                //ввели неправильный пароль
                clearField()
                
                subtitleLabel?.text = "Неверный пароль"
                asyncAfter(milliseconds: 1000, {
                    let new = AppendedCardViewController.createOne()
                    new.1?.needsOpenChat = self.needsOpenChat
                    self.needsOpenChat = false
                    self.tabBarController?.viewControllers?[0] = new.0
                })
            }
        case .enter:
            if PasswordsManager.instance.currentPassword == enteredPassword {
                let new = AppendedCardViewController.createOne()
                new.1?.needsOpenChat = self.needsOpenChat
                self.needsOpenChat = false
                self.tabBarController?.viewControllers?[0] = new.0
            } else {
                
                //ввели неправильный пароль
                clearField()
            }
        }
    }
    
    func clearField() {
        self.view.endEditing(true)
        shakeDots {
            self.field.text = ""
            self.field.currentNumber = []
            self.updateDots()
        }
        
    }
    
    func shakeDots(completion: @escaping ()->Void) {
        UIView.animate(withDuration: 0.05, animations: {
            self.stack.frame.origin.x -= 10
        }) { (_) in
            UIView.animate(withDuration: 0.05, animations: {
                self.stack.frame.origin.x += 10
            }) { (_) in
                UIView.animate(withDuration: 0.05, animations: {
                    self.stack.frame.origin.x -= 10
                }) { (_) in
                    UIView.animate(withDuration: 0.05, animations: {
                        self.stack.frame.origin.x += 10
                    }) { (_) in
                        UIView.animate(withDuration: 0.05, animations: {
                            self.stack.frame.origin.x -= 10
                        }) { (_) in
                            UIView.animate(withDuration: 0.05, animations: {
                                self.stack.frame.origin.x += 10
                            }) { (_) in
                                completion()
                            }
                        }
                    }
                }
            }
        }
    }
    
}

extension PasswordViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let newText = ((textField.text ?? "") as NSString).replacingCharacters(in: range, with: string)
        asyncAfter(milliseconds: 10) {
            self.updateDots()
        }
        return newText.count < 5
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        cancelButton.isHidden = false
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        cancelButton.isHidden = true
    }
}
