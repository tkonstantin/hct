//
//  User.swift
//  HCT
//
//  Created by Константин on 26.10.2017.
//  Copyright © 2017 Константин. All rights reserved.
//

import Foundation


class User: NSObject, NSCoding {
    var id = ""
    
    init?(id _id: String?) {
        guard _id != nil else { return nil }
        id = _id!
    }
    
    required init?(coder aDecoder: NSCoder) {
        self.id = aDecoder.decodeObject(forKey: "id") as? String ?? ""
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(id, forKey: "id")
    }
}


