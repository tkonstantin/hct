//
//  Service.swift
//  HCT
//
//  Created by Константин on 23.10.2017.
//  Copyright © 2017 Константин. All rights reserved.
//

import UIKit

@objc(Service)
class Service: NSObject, NSCoding {
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(self.title, forKey: "title")
        aCoder.encode(self.icon, forKey: "icon")
        aCoder.encode(self.image, forKey: "image")
    }
    
    required init?(coder aDecoder: NSCoder) {
        self.title = aDecoder.decodeObject(forKey: "title") as? String ?? ""
        self.icon = aDecoder.decodeInteger(forKey: "icon")
        self.image = aDecoder.decodeObject(forKey: "image") as? UIImage ?? UIImage()
    }
    
    var title: String = ""
    var icon: Int = 0
    var image = UIImage()

    init(with json: NSDictionary) {
        self.title = json["title"] as? String ?? ""
        self.icon = json["icon"] as? Int ?? 0
    }
    
    init(_ _title: String, icon _icon: Int) {
        self.title = _title
        self.icon = _icon
    }
}
