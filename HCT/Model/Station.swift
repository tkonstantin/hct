//
//  Stantion.swift
//  HCT
//
//  Created by Константин on 23.10.2017.
//  Copyright © 2017 Константин. All rights reserved.
//

import Foundation
import CoreData

@objc(Station)
class Station: NSManagedObject, GMUClusterItem {
    
    convenience init(_ insert: Bool) {
        self.init(entity: Station.entitys, insertInto: insert ? CoreDataService.instance.privateContext : nil)
    }
    
    class var entitys: NSEntityDescription {
        var entity = NSEntityDescription()
        entity = NSEntityDescription.entity(forEntityName: "Station", in: CoreDataService.instance.privateContext)!
        return entity
    }
    
    
    var position: CLLocationCoordinate2D {
        get {
            return CLLocationCoordinate2D(latitude: self.lat, longitude: self.lon)
        }
    }
    
    
    @NSManaged var id64: Int64
    
    var id: Int {
        get {
            return Int(id64 % Int64(Int.max))
        } set {
            id64 = Int64(newValue)
        }
    }
    @NSManaged var title: String
    @NSManaged var brand: String
    @NSManaged var addr: String
    @NSManaged var lat: Double
    @NSManaged var lon: Double
    @NSManaged var services: [Service]
    @NSManaged var fuels: [Fuel]
    @NSManaged var isFavorite: Bool
    @NSManaged var distanceCached: Double
    @NSManaged var distanceCachedRounded: Int64
    
    var distanceLabel: String {
        get {
            return distance.distanceString
        }
    }
    
    var distance: Double? {
        get {
            guard distanceCached == 0 else { return distanceCached }
            if let myLoc = UserDataManager.locationManager.location {
                distanceCached = CLLocation(latitude: self.lat, longitude: self.lon).distance(from: myLoc)
                distanceCachedRounded = Int64(distanceCached/1000)
                return distanceCached
            } else {
                return nil
            }
        }
    }
    
    func calculateDistance(completion: @escaping ()->Void) {
        asyncAfter { [weak self] in
            if let myLoc = UserDataManager.locationManager.location {
                self?.distanceCached = self!.cllocation.distance(from: myLoc)
                self?.distanceCachedRounded = Int64(self!.distanceCached/1000)
            }
            completion()
        }

    }
    
    
    var cllocation: CLLocation {
        get {
            return CLLocation(latitude: self.lat, longitude: self.lon)
        }
    }
    
    func loaded(with json: NSDictionary) -> Station {
        self.id64 = json["id"] as? Int64 ?? 0
        self.title = json["title"] as? String ?? ""
        self.brand = json["brand"] as? String ?? ""
        self.addr = json["addr"] as? String ?? ""
        
        let latValue = json["lat"]
        self.lat = latValue as? Double ?? Double(latValue as? String ?? "0") ?? 0
        
        let lonValue = json["lon"]
        self.lon = lonValue as? Double ?? Double(lonValue as? String ?? "0") ?? 0
        
        if let fuelsDicts = json["fuels"] as? [NSDictionary] {
            for fuelsDict in fuelsDicts {
                self.fuels.append(Fuel(with: fuelsDict))
            }
        }
        
        if let servicesDicts = json["services"] as? [NSDictionary] {
            for servicesDict in servicesDicts {
                self.services.append(Service(with: servicesDict))
            }
        }
        return self
        
    }
    
}


extension Optional where Wrapped == Double {
    var distanceString: String {
        get {
            if let dist = self {
                if dist > 500 {
                    return "\((dist/1000).roundTo(x: 2)) км"
                } else {
                    return "\(dist.roundTo(x: 1)) м"
                }
            } else {
                return "Расстояние неизвестно"
            }
        }
    }
}

extension Double {
    var distanceString: String {
        get {
            if self > 500 {
                return "\((self/1000).roundTo(x: 2)) км"
            } else {
                return "\(self.roundTo(x: 1)) м"
            }
        }
    }
}
