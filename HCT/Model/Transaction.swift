//
//  Transaction.swift
//  HCT
//
//  Created by Константин on 24.10.2017.
//  Copyright © 2017 Константин. All rights reserved.
//

import Foundation

class Transaction {
    
    private enum keys {
        static let id = "id"
        static let stationID = "aid"
        static let addr = "addr"
        static let brand = "brand"
        static let name = "name"
        static let type = "type"
        static let fuel = "fuel"
        static let value = "value"
        static let sum = "amount"
        static let bonus = "add_amount"
        static let date = "dt"
        static let addText = "addText"
        static let text = "text"
        
        
    }
    
    enum types: Int {
        case requested = 1
        case rejected
        case appended
        case purchased
        case cancelled
        case bonused
        case returned
        case undefined
    }
    
    var id: String = ""
    var date: Date = Date()
    var sum: Double = 0.0
    var addr: String = ""
    var brand: String = ""
    var name: String = ""
    var type: types = .undefined
    var fuel: String = ""
    var value: Double = 0
    var bonus: Double = 0
    var stationID: Int = 0
    var addText: String = ""
    var text: String = ""
    
    var currency = "₽"
    var comment: String {
        get {
            return text
        }
    }
    
    var isPositive: Bool {
        get {
            switch type {
            case .appended, .bonused, .returned: return true
            default: return false
            }
        }
    }
    
    var isNegative: Bool {
        get {
            switch type {
            case .purchased: return true
            default: return false
            }
        }
    }
    var rightSideColor: UIColor {
        get {
            if isPositive {
                return UIColor(hex: "58A800")
            }
            if isNegative {
                return UIColor(hex: "D0021B")
            }
            return .black
        }
    }
    
    
    init(with json: NSDictionary) {
        self.id = json[keys.id] as? String ?? ""
        self.date = Date(timeIntervalSince1970: (json[keys.date] as? Double ?? 0)/1000)
        self.sum = json[keys.sum] as? Double ?? 0
        self.addr = json[keys.addr] as? String ?? ""
        self.brand = json[keys.brand] as? String ?? ""
        self.name = json[keys.name] as? String ?? ""
        self.type = types(rawValue: json[keys.type] as? Int ?? -1) ?? .undefined
        self.fuel = json[keys.fuel] as? String ?? ""
        self.value = json[keys.value] as? Double ?? 0
        self.bonus = json[keys.bonus] as? Double ?? 0
        self.stationID = json[keys.stationID] as? Int ?? 0
        self.text = json[keys.text] as? String ?? ""
        self.addText = json[keys.addText] as? String ?? ""
        
    }
}


