//
//  Message.swift
//  HCT
//
//  Created by Константин on 14/12/2018.
//  Copyright © 2018 Константин. All rights reserved.
//

import Foundation

class Message: NSObject, NSCoding {
    var text: String?
    var image: String?
    var image_local: UIImage?
    var date: Date = Date()
    var incoming: Bool = false
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(date, forKey: "date")
        aCoder.encode(image_local, forKey: "image_local")
        aCoder.encode(incoming, forKey: "incoming")
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        date = aDecoder.decodeObject(forKey: "date") as? Date ?? Date()
        image_local = aDecoder.decodeObject(forKey: "image_local") as? UIImage
        incoming = aDecoder.decodeObject(forKey: "incoming") as? Bool ?? false
        
    }
    
    init(with json: NSDictionary) {
        let message = json["message"] as? String //от водителя
        let answer = json["answer"] as? String //от оператора
        let c_date = json["cdat"] as? Int64 //от водителя
        //        let a_date = json["adat"] as? Int //от оператора
        
        if c_date != nil {
            incoming = message == nil
            text = message ?? answer
            date = Date.init(timeIntervalSince1970: Double(c_date!/1000))
            image = nil
        }
    }
    
    init(createdWith msg: String, isIncoming: Bool = false) {
        incoming = isIncoming
        text = msg
        date = Date()
    }
    
    
    init(createdWith image: UIImage) {
        incoming = false
        image_local = image
        date = Date()
    }
}
