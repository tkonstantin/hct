//
//  Fuel.swift
//  HCT
//
//  Created by Константин on 23.10.2017.
//  Copyright © 2017 Константин. All rights reserved.
//

import Foundation

@objc(Fuel)
class Fuel: NSObject, NSCoding {
    func encode(with aCoder: NSCoder) {
        aCoder.encode(self.title, forKey: "title")
        aCoder.encode(self.price, forKey: "price")
    }
    
    required init?(coder aDecoder: NSCoder) {
        self.title = aDecoder.decodeObject(forKey: "title") as? String ?? ""
        self.price = aDecoder.decodeDouble(forKey: "price")
    }
    
    var title: String = ""
    var price: Double = 0
    
    init(_ title: String) {
        self.title = title
    }
    
    init(with json: NSDictionary) {
        self.title = json["title"] as? String ?? ""
        self.price = json["price"] as? Double ?? Double((json["price"] as? String ?? "0")) ?? 0
    }
}
