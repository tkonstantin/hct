//
//  Card.swift
//  HCT
//
//  Created by Константин on 25.10.2017.
//  Copyright © 2017 Константин. All rights reserved.
//

import Foundation


class Card: NSObject, NSCoding {
    
    private enum keys {
        static let name = "name"
        static let comp = "comp"
        static let number = "card"
        static let status = "status"
        static let balance = "balance"

    }
    enum statuses: Int {
        case ok = 0
        case blockedByUser
        case blockedBySystem
        case blockedByCompany
        case serviceBreaked
    }
    

    
    var isBlockedByUser: Bool {
        get {
            return status == .blockedByUser
        }
    }
    
    var isBlockedBySystem: Bool {
        get {
            return status == .blockedBySystem || status == .blockedByCompany
        }
    }
    
    var isServiceBreaked: Bool {
        get {
            return status == .serviceBreaked
        }
    }
    
    
    
    var number: String = "****"
    var balance: Double = 73.73
    var status: statuses = .ok
    var name = ""
    var comp = ""
    
    var currency: String = " ₽"

    
    init(with json: NSDictionary) {
        self.number = json[keys.number] as? String ?? ""
        self.balance = json[keys.balance] as? Double ?? Double(json[keys.number] as? String ?? "0") ?? 0
        self.status = statuses(rawValue: json[keys.status] as? Int ?? 0) ?? .ok
        self.name = json[keys.name] as? String ?? ""
        self.comp = json[keys.comp] as? String ?? ""

    }
    
    
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(self.number, forKey: keys.number)
        aCoder.encode(self.balance, forKey: keys.balance)
        aCoder.encode(self.status.rawValue, forKey: keys.status)
        aCoder.encode(self.name, forKey: keys.name)
        aCoder.encode(self.comp, forKey: keys.comp)

    }
    
    required init?(coder aDecoder: NSCoder) {
        self.number = aDecoder.decodeObject(forKey: keys.number) as? String ?? "****"
        self.balance = aDecoder.decodeDouble(forKey: keys.balance)
        self.status = statuses(rawValue: aDecoder.decodeInteger(forKey: keys.status)) ?? .ok
        self.name = aDecoder.decodeObject(forKey: keys.name) as? String ?? ""
        self.comp = aDecoder.decodeObject(forKey: keys.comp) as? String ?? ""

    }
    
    
}
