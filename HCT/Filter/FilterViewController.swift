//
//  FilterViewController.swift
//  HCT
//
//  Created by Константин on 23.10.2017.
//  Copyright © 2017 Константин. All rights reserved.
//

import UIKit

class FilterViewController: UIViewController {

    @IBOutlet weak var servicesCollection: UICollectionView!
    @IBOutlet weak var fuelsCollection: UICollectionView!
    @IBOutlet weak var fuelsHeight: NSLayoutConstraint!
    @IBOutlet weak var servicesHeight: NSLayoutConstraint!
    
    var fuels: [Fuel] {
        get {
            return UserDataManager.allFuels
        }
    }
    
    var services: [Service] {
        get {
            return UserDataManager.allServices
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        servicesCollection.delegate = self
        servicesCollection.dataSource = self
        fuelsCollection.delegate = self
        fuelsCollection.dataSource = self
        
        fillObjects()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    private func fillObjects() {

        
        self.servicesCollection.reloadData()
        self.fuelsCollection.reloadData()
        
        asyncAfter {
            self.fuelsHeight.constant = self.fuelsCollection.contentSize.height
            self.servicesHeight.constant = self.servicesCollection.contentSize.height
        }
        

        
        self.view.setNeedsLayout()
        self.view.layoutIfNeeded()
    }

    @IBAction func reset(_ sender: UIButton) {
        FilterDataManager.current.settings.fuels = []
        FilterDataManager.current.settings.services = []
        self.fuelsCollection.reloadData()
        self.servicesCollection.reloadData()
    }
}

extension FilterViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == fuelsCollection {
            return fuels.count
        } else {
            return services.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == fuelsCollection {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! FuelCollectionViewCell
            cell.name.text = fuels[indexPath.item].title
            cell.backgroundColor = FilterDataManager.current.settings.fuels.contains(where: { $0.title == fuels[indexPath.item].title}) ? redColor : .white
            cell.name.textColor = FilterDataManager.current.settings.fuels.contains(where: { $0.title == fuels[indexPath.item].title}) ? .white : darkGrayColor
            return cell
        } else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! ServiceCollectionViewCell
            cell.icon.image = services[indexPath.item].image.withRenderingMode(.alwaysTemplate)
            cell.title.text = services[indexPath.item].title
            cell.iconView.backgroundColor = FilterDataManager.current.settings.services.contains(where: {$0.icon == services[indexPath.item].icon}) ? redColor : .white
            cell.icon.tintColor = FilterDataManager.current.settings.services.contains(where: {$0.icon == services[indexPath.item].icon}) ? .white : darkGrayColor
            return cell
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == fuelsCollection {
            if let index = FilterDataManager.current.settings.fuels.index(where: { $0.title == fuels[indexPath.item].title}) {
                FilterDataManager.current.settings.fuels.remove(at: index)
            } else {
                FilterDataManager.current.settings.fuels.append(fuels[indexPath.item])
            }
        } else {
            let currentService = services[indexPath.item]
            
            if let index = FilterDataManager.current.settings.services.index(where: {$0.icon == currentService.icon}) {
                FilterDataManager.current.settings.services.remove(at: index)
            } else {
                FilterDataManager.current.settings.services.append(currentService)
            }

        }
        
        collectionView.reloadData()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == fuelsCollection {
            return CGSize(width: (self.view.bounds.width-32)/3 - 16, height: 43)
        } else {
            return CGSize(width: self.view.bounds.width/3, height: 120)
        }
    }
    
    
}
