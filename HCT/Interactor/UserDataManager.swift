//
//  UserDataManager.swift
//  HCT
//
//  Created by Константин on 23.10.2017.
//  Copyright © 2017 Константин. All rights reserved.
//

import Foundation
import CoreLocation


class UserDataManager {
    
    static let current = UserDataManager()
    static let locationManager = CLLocationManager()
    
    //    private let previousCalculatedLocationKey = NSString(string: "previousCalculatedLocationKey")
    private let previousCalculatedLocationKey = "previousCalculatedLocationKey"
    
    private var previousCalculatedLocation: CLLocation? {
        get {
            if let data = UserDefaults.standard.value(forKey: previousCalculatedLocationKey) as? Data {
                return NSKeyedUnarchiver.unarchiveObject(with: data) as? CLLocation
            }
            return nil
        } set {
            let data = newValue == nil ? Data() : NSKeyedArchiver.archivedData(withRootObject: newValue!) as Data
            UserDefaults.standard.set(data, forKey: previousCalculatedLocationKey)
        }
    }
    
    func calculateDistances(completion: @escaping ()->Void) {
        
        if shouldUpdateDistances {
            DispatchQueue.global().sync {
                let stations = Server.makeRequest.all_stations
                self.calculateAllDistances(in: stations) {
                    self.previousCalculatedLocation = UserDataManager.locationManager.location
                    asyncAfter {
                        CoreDataService.instance.save()
                        completion()
                    }
                }
            }
        } else {
            completion()
        }
        
    }

    
    private func calculateAllDistances(for index: Int=0, in stations: [Station], completion: @escaping ()->Void) {
        guard index < stations.count else { completion(); return }
        stations[index].calculateDistance {
            self.calculateAllDistances(for: index + 1, in: stations, completion: completion)
        }
    }
    
    private var shouldUpdateDistances: Bool {
        get {
            guard previousCalculatedLocation != nil else { return true }
            if let distanceFromPreviousLocation = UserDataManager.locationManager.location?.distance(from: previousCalculatedLocation!) {
                return distanceFromPreviousLocation > 300
            } else {
                return true
            }
        }
    }
    
    func reset() {
        pan = nil
        user = nil
        card = nil
        sendedImageMessages = []
        Server.makeRequest.reset()
    }
    
    var pan: String? {
        get {
            let res = String(data: KeychainService.loadData(for: "sp") as Data? ?? Data(), encoding: String.Encoding.utf8)
            return res
        } set {
            KeychainService.save(data: newValue?.data(using: String.Encoding.utf8) as NSData? ?? NSData(), for: "sp")
        }
    }
    
    var user: User? {
        get {
            if let data = KeychainService.loadData(for: "sup") as Data? {
                return NSKeyedUnarchiver.unarchiveObject(with: data) as? User
            }
            return nil
        } set {
            KeychainService.save(data: NSKeyedArchiver.archivedData(withRootObject: newValue ?? NSData()) as NSData, for: "sup")
        }
    }
    
    var card: Card? {
        get {
            if let data = KeychainService.loadData(for: "suc") as Data? {
                return NSKeyedUnarchiver.unarchiveObject(with: data) as? Card
            }
            return nil
        } set {
            KeychainService.save(data: NSKeyedArchiver.archivedData(withRootObject: newValue ?? NSData()) as NSData, for: "suc")
        }
    }
    
    
    
}



extension UserDataManager {
    static private var cachedFuels = [Fuel]()
    static private var cachedServices = [Service]()
    
    static var allFuels: [Fuel] {
        get {
            if cachedFuels.count == 0 {
                if let data = KeychainService.loadData(for: NSString(string: "allfueldcached")) as Data? {
                    cachedFuels =  NSKeyedUnarchiver.unarchiveObject(with: data) as? [Fuel] ?? []
                }
            }
            return cachedFuels
        } set {
            cachedFuels = newValue
            KeychainService.save(data: NSKeyedArchiver.archivedData(withRootObject: newValue) as NSData, for: NSString(string: "allfueldcached"))
        }
    }
    static var allServices: [Service] {
        get {
            if cachedServices.count == 0 {
                if let data = KeychainService.loadData(for: NSString(string: "allservicescached")) as Data? {
                    cachedServices =  NSKeyedUnarchiver.unarchiveObject(with: data) as? [Service] ?? []
                }
            }
            return cachedServices
        } set {
            cachedServices = newValue
            KeychainService.save(data: NSKeyedArchiver.archivedData(withRootObject: newValue) as NSData, for: NSString(string: "allservicescached"))
        }
    }
}

var sendedImageMessages: [Message] {
    get {
        if let data  = UserDefaults.standard.value(forKey: "sended_image_messages_01") as? Data {
            return NSKeyedUnarchiver.unarchiveObject(with: data) as? [Message] ?? []
        }
        return []
    } set {
        UserDefaults.standard.set(NSKeyedArchiver.archivedData(withRootObject: newValue), forKey: "sended_image_messages_01")
        _ = UserDefaults.standard.synchronize()
    }
}
