//
//  Server.swift
//  HCT
//
//  Created by Константин on 23.10.2017.
//  Copyright © 2017 Константин. All rights reserved.
//

import Foundation
import Alamofire


class Server {
    static let makeRequest = Server()
    
    var all_stations: [Station] = []
    var receivedMessagesCount = UIApplication.shared.applicationIconBadgeNumber {
        didSet {
            UIApplication.shared.applicationIconBadgeNumber = receivedMessagesCount
        }
    }
    
    private var pushToken: String?
    
    func setPushToken(token: String) {
        pushToken = token
        self.updateStatus { _ in}
    }
    
    func reset() {
        pushToken = nil
        lastStatusUpdateDate = nil
        
    }
    
     var lastStatusUpdateDate: Double? {
        get {
            return UserDefaults.standard.double(forKey: "last_sync_date_key1")
        }
        set {
            UserDefaults.standard.set(newValue, forKey: "last_sync_date_key1")
            _ = UserDefaults.standard.synchronize()
        }
    }
    
    private var deviceID: String? {
        get {
            return UIDevice.current.identifierForVendor?.uuidString
        }
    }
    
    func updateStatus(completion: @escaping (updateStatuses?)->Void) {
        all_stations = CoreDataService.instance.allStations()
        
        print("pushToken = \(pushToken)")
        
        var params: [String: AnyObject] = [:]
        params["app"] = "1.0" as AnyObject
        params["osid"] = UIDevice.current.systemName as AnyObject
        params["osver"] = UIDevice.current.systemVersion as AnyObject
        params["vid"] = "Apple" as AnyObject
        params["mid"] = UIDevice.current.model as AnyObject
        params["device_id"] = deviceID as AnyObject
        params["nid"] = pushToken as AnyObject
        params["dt"] = (lastStatusUpdateDate ?? 0)*1000 as AnyObject
        
        Alamofire.request(url_paths.base + url_paths.updateStatus, method: .post, parameters: params, encoding: JSONEncoding.default, headers: nil).responseJSON { (response) in

            print("downloaded at \(Date().timeIntervalSince1970)")
            if let json = response.json, let update = json["update"] as? Int {
                
                if self.all_stations.count == 0  {
                    for add in (json["add"] as? [NSDictionary]) ?? [] {
                        self.all_stations.append(Station(true).loaded(with: add))
                    }
                } else {
                    for add in (json["add"] as? [NSDictionary]) ?? [] {
                        let tempStation = Station(false).loaded(with: add)
                        if self.all_stations.index(where: {$0.id == tempStation.id}) == nil  {
                            self.all_stations.appendSafely(newValues: [Station(true).loaded(with: add)])
                        }
                    }
                }
                
                CoreDataService.instance.save()
                
                CoreDataService.instance.clearStations(with: json["del"] as? [NSDictionary])

                CoreDataService.instance.clearStations(with: json["chg"] as? [NSDictionary])

                for chg in (json["chg"] as? [NSDictionary]) ?? [] {
                    self.all_stations.append(Station(true).loaded(with: chg))
                }

                self.lastStatusUpdateDate = Date().timeIntervalSince1970
                print("completed processing at \(Date().timeIntervalSince1970)")
               completion(updateStatuses(update))
            } else {
                completion(nil)
            }
        }
    }
    
    func regStepOne(card: String, completion: @escaping ((Bool, String?, String?)->Void)) {
        var params: [String: AnyObject] = ["card": card as AnyObject]
        params["device_id"] = deviceID as AnyObject
        Alamofire.request(url_paths.base + url_paths.regStepOne , method: .post, parameters: params, encoding: JSONEncoding.default, headers: nil).responseJSON { (response) in
        
            
            if let json = response.json, let res = json["res"] as? Int {
                completion(res == 0, res.regStepOneMessage, json["cap"] as? String)
            } else {
                completion(false, "Неизвестная ошибка", nil)
            }
        }
    }
    
    func regStepTwo(cap: String, code: String, completion: @escaping ((Bool, String?, String?)->Void)) {
        var params: [String: AnyObject] = ["cap": cap as AnyObject]
        params["code"] = code as AnyObject
        params["device_id"] = deviceID as AnyObject

        Alamofire.request(url_paths.base + url_paths.regStepTwo, method: .post, parameters: params, encoding: JSONEncoding.default, headers: nil).responseJSON { (response) in

            if let json = response.json, let res = json["res"] as? Int {
                //TODO: - запонмить свой user_id и запросить какие-то данные о пользователе
                UserDataManager.current.user = User(id: json["user_id"] as? String)
                completion(res == 0, res.regStepTwoMessage, json["user_id"] as? String)
            } else {
                completion(false, "Неизвестная ошибка", nil)
            }
        }
    }
    
    func getServiceList() {
        Alamofire.request(url_paths.base + url_paths.getServiceList, method: .post, parameters: nil, encoding: JSONEncoding.default, headers: nil).responseJSON { (response) in
            var services: [Service] = []
            
            if let json = response.result.value as? NSDictionary, let items = json["data"] as? [NSDictionary] {
                for item in items {
                    services.append(Service(with: item))
                }
                
                self.getServiceIcons(completion: { (icons) in
                    for icon in icons {
                        if let index = services.index(where: {$0.icon == icon.key}) {
                            services[index].image = icon.value
                        }
                    }
                    UserDataManager.allServices = services

                })
            }
        }
    }
    
    func getFuelList() {
        Alamofire.request(url_paths.base + url_paths.getFuelList, method: .post, parameters: nil, encoding: JSONEncoding.default, headers: nil).responseJSON { (response) in

            var fuels: [Fuel?] = []
            if let json = response.result.value as? NSDictionary, let items = json["data"] as? [String] {
                for item in items {
                    fuels.append(Fuel(item))
                }
                UserDataManager.allFuels = fuels.flatMap({$0})
            }
        }
    }
    
    func getServiceIcons(completion: @escaping ([Int: UIImage])->Void) {
        Alamofire.request(url_paths.base + url_paths.getServiceIcons, method: .post, parameters: ["id": 0], encoding: JSONEncoding.default, headers: nil).responseJSON { (response) in
            var result: [Int: UIImage] = [:]
            if let json = response.result.value as? NSDictionary, let data = json["data"] as? NSDictionary {
                for key in data.allKeys {
                    result[Int(key as! String) ?? 0] = UIImage(data: Data(base64Encoded: data[key] as! String) ?? Data())
                }
            }
            
            completion(result)
        }
    }
    
    
    
    func getTransactions(page: Int = 0, completion: @escaping ([Transaction])->Void) {
        var params: [String: String] = ["user_id": UserDataManager.current.user?.id ?? ""]
        params["device_id"] = deviceID
        params["pan"] = UserDataManager.current.pan
        params["page"] = String(page)
        
        

        Alamofire.request(url_paths.base + url_paths.getTransactions, method: .post, parameters: params, encoding: JSONEncoding.default, headers: nil).responseJSON { (response) in
            var transactions: [Transaction] = []
            if let json = response.json, let trx = json["trx"] as? [NSDictionary] {
                for dict in trx {
                    transactions.append(Transaction(with: dict))
                }
            }
            completion(transactions)
        }
    }
    
    func blockCurrentCard(completion: @escaping (Bool)->Void) {
        guard UserDataManager.current.user != nil else { completion(false); return }
        
        var params: [String: Any] = [:]
        params["device_id"] = deviceID
        params["user_id"] = UserDataManager.current.user!.id
        params["pan"] = UserDataManager.current.pan
        
        Alamofire.request(url_paths.base + url_paths.blockCard, method: .post, parameters: params, encoding: JSONEncoding.default, headers: nil).responseJSON { (response) in
            if let json = response.json, let res = json["res"] as? Int {
                completion(res == 0)
            } else {
                completion(false)
            }
        }
        
        
    }
    
    func getUser(user_id: String, completion: @escaping ()->Void) {
        var params: [String: Any] = [:]
        params["device_id"] = deviceID
        params["user_id"] = user_id
        params["pan"] = UserDataManager.current.pan
        
        Alamofire.request(url_paths.base + url_paths.getUser, method: .post, parameters: params, encoding: JSONEncoding.default, headers: nil).responseJSON { (response) in

            if let json = response.json {
                UserDataManager.current.card = Card(with: json)
            }
            completion()
        }
    }
    
    
    func refill(summ: String, completion: @escaping (Bool, String?)->Void) {
        var params: [String: Any] = [:]
        params["amount"] = summ
        params["device_id"] = deviceID
        params["user_id"] = UserDataManager.current.user!.id
        params["pan"] = UserDataManager.current.pan

        Alamofire.request(url_paths.base + url_paths.refill, method: .post, parameters: params, encoding: JSONEncoding.default, headers: nil).responseJSON { (response) in
            if let json  = response.json, let res = json["res"] as? Int {
                completion(res == 0, res.refillMessage)
            } else {
                completion(false, "Неизвестная ошибка")
            }
        }


    }
    
    
    func sendFeeedback(_ msg: String, completion: @escaping (Bool, String?)->Void) {
        var params: [String: Any] = [:]
        params["text"] = msg
        params["device_id"] = deviceID
        params["user_id"] = UserDataManager.current.user!.id

        
        Alamofire.request(url_paths.base + url_paths.feedback, method: .post, parameters: params, encoding: JSONEncoding.default, headers: nil).responseJSON { (response) in
            if let json = response.json, let res = json["res"] as? Int {
                completion(res == 0, res.feedbackMessage)
            } else {
                completion(false, "Неизвестная ошибка")
            }
        }
        
    }
    
    func sendFile(_ file: Any, completion: @escaping (Bool)->Void) {
        
        var compressedData: Data!
        
        if let img = file as? UIImage {
            compressedData = UIImageJPEGRepresentation(img, 0.75)
        }
        
        guard compressedData != nil else { completion(false); return }
        
        Alamofire.upload(multipartFormData: { (formdata) in
            formdata.append(compressedData, withName: "photo", fileName: "photo", mimeType: "image/jpeg")
        }, to: url_paths.base + url_paths.feedbackPhoto + "?device_id=\(deviceID!)&user_id=\(UserDataManager.current.user!.id)", method: .post, headers: nil) { (res) in
            switch res {
            case .success(let request, _, _):
                request.responseJSON(completionHandler: { (response) in

                    if let json = response.result.value as? NSDictionary {
                        completion(json["res"] as? Int == 0)
                    } else {
                        completion(false)
                    }
                })
            case .failure(let error):
                print("upload file encoding failed with error: \(error)")
                completion(false);
                return
            }
        }
    }
    
    func getMessagesHistory(page: Int, pageSize: Int = historyPageSize, completion: @escaping ([Message])->Void) {
        var params: [String: Any] = [:]
        params["deviceId"] = deviceID
        params["page"] = page
        params["itemsPerPage"] = pageSize

        
        Alamofire.request(url_paths.base + url_paths.chatHistory, method: .post, parameters: params, encoding: JSONEncoding.default, headers: nil).responseJSON { (response) in
            var messages: [Message] = []
            
            if let json = response.result.value as? NSDictionary, let messagesArr = json["messages"] as? [NSDictionary] {
                for dict in messagesArr {
                    messages.append(Message(with: dict))
                }
            }
            completion(messages)
        }
        
    }
    
    
    
    
    enum url_paths {
        static let base = "https://dev.nst.msk.ru/mobile/"
        static let updateStatus = "getStatus"
        static let regStepOne = "regStepOne"
        static let regStepTwo = "regStepTwo"
        static let getTransactions = "getUserTrx"
        static let getUser = "getUser"
        static let refill = "cardTopup"
        static let blockCard = "cardBlock"
        static let feedback = "sendMessage"
        static let feedbackPhoto = "sendPhoto"
        static let getServiceList = "getServiceList"
        static let getServiceIcons = "getIcons"
        static let getFuelList = "getFuelList"
        static let chatHistory = "getMessages"
        
    }
    
    enum updateStatuses {
        case upToDate
        case needsAlert
        case blocked
        
        init(_ int: Int) {
            switch int {
            case 1: self = .needsAlert
            case 2: self = .blocked
            default: self = .upToDate
            }
        }

    }
    
}



extension DataResponse {
    var json: NSDictionary? {
        get {
            return self.result.value as? NSDictionary
        }
    }
}


extension Int {
    fileprivate var regStepOneMessage: String? {
        get {
            switch self {
            case 1: return "Неверный номер карты"
            case 2: return "Карта заблокирована"
            case 3: return "Карта не поддерживается"
            case 4: return "Внутренняя ошибка. Попробуйте повторить позже"
            default: return nil
            }
        }
    }
    
    fileprivate var regStepTwoMessage: String? {
        get {
            switch self {
            case 1: return "Карта заблокирована пользователем"
            case 2: return "Карта заблокирована системой"
            case 3: return "Внутренняя ошибка. Попробуйте повторить позже"
            case 4: return "Нет пользователя с device или user id"
            case 5: return "Нет пользователя с device и user id"
            default: return nil
            }
        }
    }
    
    fileprivate var refillMessage: String? {
        get {
            switch self {
            case 1: return "Карта заблокирована пользователем"
            case 2: return "Карта заблокирована системой"
            case 3: return "Внутренняя ошибка. Попробуйте повторить позже"
            case 4: return "Нет пользователя с device или user id"
            case 5: return "Нет пользователя с device и user id"
            default: return nil
            }
        }
    }
    
    fileprivate var feedbackMessage: String? {
        get {
            switch self {
            case 3: return "Внутренняя ошибка. Попробуйте повторить позже"
            case 4: return "Нет пользователя с device или user id"
            case 5: return "Нет пользователя с device и user id"
            default: return nil
            }
        }
    }
    
}

