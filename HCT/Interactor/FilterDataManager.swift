//
//  FilterDataManager.swift
//  HCT
//
//  Created by Константин on 23.10.2017.
//  Copyright © 2017 Константин. All rights reserved.
//

import Foundation


class FilterDataManager {
    
    static let current = FilterDataManager()
    
    typealias filter = (fuels: [Fuel], services: [Service])
    
    var settings: filter = ([], []) {
        didSet {
            needsApplyForMap = true
            needsApplyForList = true
        }
    }
    
    var needsApplyForMap = true
    var needsApplyForList = true
    
    var isFiltersActive: Bool {
        get {
            return settings.fuels.count != 0 || settings.services.count != 0
        }
    }
    
}
