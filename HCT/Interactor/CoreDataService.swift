//
//  CoreDataService.swift
//  UPS
//
//  Created by Константин Черкашин on 18.10.16.
//  Copyright © 2016 Константин Черкашин. All rights reserved.
//

import Foundation
import CoreData


class CoreDataService: NSObject {
    class var instance: CoreDataService {
        struct Singleton {
            static let instance = CoreDataService()
        }
        return Singleton.instance
    }
    
    let coordinator: NSPersistentStoreCoordinator
    var model: NSManagedObjectModel
    var privateContext: NSManagedObjectContext
    
    fileprivate override init() {
        let modelURL = Bundle.main.url(forResource: "Model", withExtension: "momd")!
        model = NSManagedObjectModel(contentsOf: modelURL)!
        
        let fileManager = FileManager.default
        let docsURL = fileManager.urls( for: .documentDirectory, in: .userDomainMask).last as URL!
        let storeURL = docsURL?.appendingPathComponent("model1.sqlite")
        
        coordinator = NSPersistentStoreCoordinator(managedObjectModel: model)
        let store = try? coordinator.addPersistentStore(ofType: NSSQLiteStoreType, configurationName: nil, at: storeURL, options: nil)
        if store == nil {
            if storeURL != nil {
                try? fileManager.removeItem(at: storeURL!)
                Server.makeRequest.lastStatusUpdateDate = nil
            }
            let newStore = try? coordinator.addPersistentStore(ofType: NSSQLiteStoreType, configurationName: nil, at: storeURL, options: nil)
            if newStore == nil {
                abort()
            }
        }
        
        try? fileManager.setAttributes([FileAttributeKey.protectionKey : FileProtectionType.completeUntilFirstUserAuthentication], ofItemAtPath: storeURL!.path)
        
        privateContext = NSManagedObjectContext(concurrencyType: NSManagedObjectContextConcurrencyType.mainQueueConcurrencyType)
        privateContext.persistentStoreCoordinator = coordinator
        super.init()
        
    }
    
    func lock(_ block:  @escaping () -> ()) {
        self.privateContext.performAndWait(block)
    }
    
    
    func save() {
        self.lock({
            try? self.privateContext.save()
        })
    }

    func clearStations(with dictsArr: [NSDictionary]?=nil) {
        if let dicts = dictsArr {
            var ids: Set<Int> = []
            for dict in dicts {
                if let id = dict["id"] as? Int {
                    ids.insert(id)
                }
            }
            guard ids.count != 0 else { return }
            let allStations = self.allStations()
            let filteredStations = allStations.filter({ids.contains($0.id)})

            for prod in filteredStations {
                self.privateContext.delete(prod)
                self.save()
            }
        } else {
            let fetch = NSFetchRequest<NSFetchRequestResult>(entityName: "Station")
            let request = NSBatchDeleteRequest(fetchRequest: fetch)
            _ = try? privateContext.execute(request)
            save()
        }
    }
    
    func closestTop() -> [Station] {
        let req: NSFetchRequest<Station> = NSFetchRequest(entityName: "Station")
        req.sortDescriptors = [NSSortDescriptor(key: "distanceCached", ascending: true)]
        
        if let results = try? self.privateContext.fetch(req) {
            return results
        } else {
            return []
        }
    }
   
    func allStations() -> [Station] {
        var returns: [Station] = []
        self.lock({
            if let results = try? self.privateContext.fetch(NSFetchRequest(entityName: "Station")) as? [Station] {
                returns = results ?? []
            }
        })
        return returns
    }
   
}
