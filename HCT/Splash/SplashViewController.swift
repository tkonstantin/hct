//
//  SplashViewController.swift
//  HCT
//
//  Created by Константин on 23.10.2017.
//  Copyright © 2017 Константин. All rights reserved.
//

import UIKit

class SplashViewController: UIViewController {
    
    override func viewDidAppear(_ animated: Bool) {
        Server.makeRequest.updateStatus { (newStatus) in
            self.showAlert(newStatus)
        }
    }
        
        private func openMain() {
            UIApplication.shared.keyWindow?.rootViewController = MainTab.createOne()
        }
        
        private func openOnboarding() {
            
        }
        
        private func showAlert(_ status: Server.updateStatuses?) {
            if status == nil || status == .upToDate {
                self.openMain()
            } else {
                let alert = UIAlertController(title: "Доступно обновление", message: status == .blocked ? "Для продолжения работы необходимо обновить приложение" : "Перейти в AppStore для обновления?", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Перейти в AppStore", style: .default, handler: { (_) in
                    UIApplication.shared.openURL(URL(string: "https://itunes.apple.com/us/app/evenly/id1274732452?l=ru&ls=1&mt=8")!)
                }))
                

                if status == .needsAlert {
                    alert.addAction(UIAlertAction(title: "Не сейчас", style: .destructive, handler: {_ in
                        self.openMain()
                    }))
                }
                self.present(alert, animated: true, completion: nil)
            }
        }
        
        override var prefersStatusBarHidden: Bool {
            get {
                return true
            }
        }
        
}
